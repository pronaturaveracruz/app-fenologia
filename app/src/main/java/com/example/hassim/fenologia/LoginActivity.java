package com.example.hassim.fenologia;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.hassim.fenologia.internet.ApiClient;
import com.example.hassim.fenologia.internet.ApiService;
import com.example.hassim.fenologia.login.SessionPrefs;
import com.example.hassim.fenologia.login.UserResponseModel;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    public static final String TAG = LoginActivity.class.getName();
    View mProgressBarGroupView;
    ProgressBar mProgressBar;
    ImageView mLogoImageView;
    View mTitleTextView, mSubtitleTextView, mLoginScrollView;
    EditText mUserEditText, mPasswordEditText;
    Button mLogInButton;
    String mCurrentUser, mCurrentPass;
    private ApiService mApiService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Objects.requireNonNull(getSupportActionBar()).hide();

        //References
        mProgressBarGroupView = findViewById(R.id.login_progress_bar_group_view);
        mProgressBar = findViewById(R.id.login_progress_bar);
        mLogoImageView = findViewById(R.id.logo_image_view);
        mTitleTextView = findViewById(R.id.login_title_text_view);
        mSubtitleTextView = findViewById(R.id.login_subtitle_text_view);
        mLoginScrollView = findViewById(R.id.login_scrollview);
        mUserEditText = findViewById(R.id.user_edit_text);
        mPasswordEditText = findViewById(R.id.password_edit_text);
        mLogInButton = findViewById(R.id.log_in_button);

        //Set the TextWatcher to each TextView
        mUserEditText.addTextChangedListener(textWatcher);
        mPasswordEditText.addTextChangedListener(textWatcher);

        //Setup Login Button
        mLogInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isOnline()) {
                    showLoginError(getString(R.string.error_network));
                    return;
                }
                //Hide keyboard
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(mLogInButton.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                }
                attemptLogin();
            }
        });

        //Create connection to ApiService
        mApiService = ApiClient.getClient().create(ApiService.class);
    }

    /* ------------------------------------------------------------------------------------- */

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mCurrentUser = mUserEditText.getText().toString().trim();
            mCurrentPass = mPasswordEditText.getText().toString().trim();
            if (!mCurrentUser.isEmpty() && !mCurrentPass.isEmpty()) {
                mLogInButton.setTextColor(getResources().getColor(R.color.colorPrimary, null));
                mLogInButton.setEnabled(true);
            } else {
                mLogInButton.setTextColor(getResources().getColor(android.R.color.darker_gray, null));
                mLogInButton.setEnabled(false);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    /* ------------------------------------------------------------------------------------- */

    private void attemptLogin() {
        mCurrentUser = mUserEditText.getText().toString().trim();
        mCurrentPass = mPasswordEditText.getText().toString().trim();
        showProgress(true);
        Call<UserResponseModel> loginCall = mApiService.login(mCurrentUser, mCurrentPass);
        loginCall.enqueue(new Callback<UserResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<UserResponseModel> call, @NonNull Response<UserResponseModel> response) {
                showProgress(false);
                if (!response.isSuccessful()) {
                    Log.e(TAG, "Ocurrio un error!");
                    return;
                }

                UserResponseModel userResponseModel = response.body();
                if ((userResponseModel != null ? userResponseModel.getStatus() : 0) == 1) {
                    Log.i(TAG, "USER OK");
                    SessionPrefs.get(LoginActivity.this).saveUser(userResponseModel);
                    goToMainActivity();
                } else {
                    Toast.makeText(getApplicationContext(), userResponseModel != null ? userResponseModel.getMessage() : null, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(@NonNull Call<UserResponseModel> call, @NonNull Throwable t) {
                Log.i(TAG, "onFailure");
                showProgress(false);
                showLoginError(t.getMessage());
            }
        });

    }

    private void showProgress(boolean show) {
        mProgressBarGroupView.setVisibility(show ? View.VISIBLE : View.GONE);
        int visibility = show ? View.GONE : View.VISIBLE;
        mLogoImageView.setVisibility(visibility);
        mTitleTextView.setVisibility(visibility);
        mSubtitleTextView.setVisibility(visibility);
        mLoginScrollView.setVisibility(visibility);
    }

    private void goToMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private void showLoginError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    private boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
        return activeNetwork != null && activeNetwork.isConnected();
    }


}
