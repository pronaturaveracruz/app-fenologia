package com.example.hassim.fenologia.details;

public class EspecieFenologia {

    private String mNombreCientifico;
    private String mNombreComun;
    private String mEtapaFenologica;

    public EspecieFenologia(String nombreCientifico, String nombreComun, String etapaFenologica) {
        mNombreCientifico = nombreCientifico;
        mNombreComun = nombreComun;
        mEtapaFenologica = etapaFenologica;
    }

    public String getNombreCientifico() {
        return mNombreCientifico;
    }

    public String getNombreComun() {
        return mNombreComun;
    }

    public String getEtapaFenologica() {
        return mEtapaFenologica;
    }
}
