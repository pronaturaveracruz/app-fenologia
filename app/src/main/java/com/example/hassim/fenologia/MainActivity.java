package com.example.hassim.fenologia;

import android.app.LoaderManager;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.hassim.fenologia.data.PhenologyContract.PrincipalEntry;
import com.example.hassim.fenologia.internet.JobSchedulerService;
import com.example.hassim.fenologia.login.SessionPrefs;
import com.example.hassim.fenologia.services.SyncSpeciesResultReceiver;
import com.example.hassim.fenologia.services.SyncSpeciesIntentService;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.holder.StringHolder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    public static final String TAG = MainActivity.class.getSimpleName();
    private static final int PRINCIPAL_LOADER = 0;
    PhenologyCursorAdapter mCursorAdapter;
    private static final int SYNC_DB_JOB_ID = 1;

    public SyncSpeciesResultReceiver receiverForSyncSpecies;

    Drawer result;

    private Toast mNoInternetToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Redirect to LoginActivity
        if (!SessionPrefs.get(this).isLoggedIn()) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            return;
        }

        //Load default layout
        setContentView(R.layout.activity_main);

        // Get current user ID and information from SharedPreferences
        SharedPreferences myShared = getSharedPreferences("FENOLOGIA_PREFS", MODE_PRIVATE);
        int currentUser = myShared.getInt("PREF_USER_ID", 99);
        String currentUserNameFromShared = myShared.getString("PREF_USER_NAME", null);
        String currentEmailFromShared = myShared.getString("PREF_USER_EMAIL", null);
        Log.i(TAG, "Current Shared: " + currentUser);

        //Setup toolbar and NavigationDrawer
        setupToolbarAndMaterialDrawer(currentUserNameFromShared, currentEmailFromShared);

        //Setup IntentService receiver
        setupSyncSpeciesResultReceiver();

        //Setup FAB
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, FormActivity.class);
                startActivity(intent);
            }
        });

        //Setup ListView
        ListView listView = findViewById(R.id.list_view);
        View emptyView = findViewById(R.id.empty_view);
        listView.setEmptyView(emptyView);
        mCursorAdapter = new PhenologyCursorAdapter(this, null);
        listView.setAdapter(mCursorAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Log.i(TAG, "Click on element in the ListView");
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                Uri currentUri = ContentUris.withAppendedId(PrincipalEntry.CONTENT_URI_WITH_JOIN, id);
                intent.setData(currentUri);
                intent.putExtra(PrincipalEntry._ID, String.valueOf(id));
                startActivity(intent);
            }
        });
        getLoaderManager().initLoader(PRINCIPAL_LOADER, null, this);

        // Start the JobScheduler
        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        if (jobScheduler != null) {
            jobScheduler.schedule(new JobInfo.Builder(SYNC_DB_JOB_ID, new ComponentName(this, JobSchedulerService.class))
                    .setPersisted(true)
                    .setPeriodic(900000 + 1000) //15 min + 1 sec (Limitation of Android N)
                    //.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)
                    .build());
        }
    }

    /* ---------------------------------------------------------------------------------------- */

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {

        Log.i(TAG, "onCreateLoader");

        String[] projection = {
                PrincipalEntry._ID + "," +
                        PrincipalEntry.COLUMN_FECHA + "," +
                        PrincipalEntry.COLUMN_LATITUD + "," +
                        PrincipalEntry.COLUMN_LONGITUD + ", " +
                        PrincipalEntry.COLUMN_SYNC_STATUS + "," +
                        PrincipalEntry.COLUMN_NUM_ESPECIES
        };

        return new CursorLoader(this,
                PrincipalEntry.CONTENT_URI_WITH_JOIN,
                projection,
                null,
                null,
                "_id DESC"
        );

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        Log.i(TAG, "onLoadFinished");
        mCursorAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.i(TAG, "onLoaderReset");
        mCursorAdapter.swapCursor(null);
    }

    /* -------------------------------------------------------------------------------------- */

    @Override
    public void onBackPressed() {
        if (result.isDrawerOpen()) {
            result.closeDrawer();
        } else {
            super.onBackPressed();
        }


    }

    /* -------------------------------------------------------------------------------------- */

    private void setupToolbarAndMaterialDrawer(String currentUserNameFromShared, String currentEmailFromShared) {
        Toolbar toolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        //#1 Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.darkGrayMD)
                .addProfiles(
                        new ProfileDrawerItem()
                                .withName(currentUserNameFromShared)
                                .withEmail(currentEmailFromShared)
                                .withIcon(R.drawable.logo_medium)
                )
                .withCompactStyle(true)
                .withProfileImagesVisible(false)
                .withSelectionListEnabledForSingleProfile(false)
                .build();
        //#2 Create elements
        final SecondaryDrawerItem item1 = new SecondaryDrawerItem()
                .withIdentifier(1)
                .withIcon(GoogleMaterial.Icon.gmd_refresh)
                .withIconColor(getResources().getColor(R.color.primaryGrayMD, null))
                .withName(R.string.update_table_species)
                .withSelectable(false)
                .withTextColor(getResources().getColor(R.color.lightGrayMD, null));
        SecondaryDrawerItem item2 = new SecondaryDrawerItem()
                .withIdentifier(2)
                .withIcon(GoogleMaterial.Icon.gmd_exit_to_app)
                .withIconColor(getResources().getColor(android.R.color.holo_red_dark, null))
                .withName(R.string.logout_item)
                .withSelectable(false)
                .withTextColor(getResources().getColor(R.color.lightGrayMD, null));
        //#3 Create the DrawerBuilder
        result = new DrawerBuilder()
                .withActivity(this)
                .withAccountHeader(headerResult)
                .withToolbar(toolbar)
                .addDrawerItems(
                        item1,
                        new DividerDrawerItem(),
                        item2
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        //Behaviour of items
                        int currentSelection = (int) drawerItem.getIdentifier();
                        switch (currentSelection) {
                            case 1:
                                Log.i(TAG, "You selected sync");
                                launchSyncSpeciesIntentService();
                                return true;
                            case 2:
                                Log.i(TAG, "You selected logout");
                                SessionPrefs.get(getApplicationContext()).logOut();
                                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                                finish();
                                return true;
                            default:
                                return false;
                        }
                    }
                })
                .withSelectedItem(-1)
                .build();
    }

    /* ---------------------------------------------------------------------------------------- */

    private void setupSyncSpeciesResultReceiver() {
        receiverForSyncSpecies = new SyncSpeciesResultReceiver(new Handler());
        receiverForSyncSpecies.setReceiver(new SyncSpeciesResultReceiver.Receiver() {
            @Override
            public void onReceiveResult(int resultCode, Bundle resultData) {

                if (resultCode == RESULT_OK) {

                    String resultValue = resultData.getString("resultValue");
                    Toast.makeText(getApplicationContext(), resultValue, Toast.LENGTH_SHORT).show();

                    //Enable again the syncButton
                    IDrawerItem item1 = result.getDrawerItem(1);
                    item1.withEnabled(true);
                    result.updateName(1, new StringHolder(R.string.update_table_species));
                    result.updateItem(item1);

                }

            }
        });
    }

    private void launchSyncSpeciesIntentService() {

        if (isOnline()) {
            //Disable syncButton to avoid multiple taps before finish.
            IDrawerItem item1 = result.getDrawerItem(1);
            item1.withEnabled(false);
            result.updateName(1, new StringHolder(R.string.syncing_message));
            result.updateItem(item1);

            Intent i = new Intent(this, SyncSpeciesIntentService.class);
            i.putExtra("receiver", receiverForSyncSpecies);
            startService(i);
        } else {
            //Avoid multiple Toasts
            if (mNoInternetToast != null) {
                mNoInternetToast.cancel();
            }
            mNoInternetToast = Toast.makeText(this, getString(R.string.error_network), Toast.LENGTH_SHORT);
            mNoInternetToast.show();
        }

    }

    private boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
        return activeNetwork != null && activeNetwork.isConnected();
    }


}
