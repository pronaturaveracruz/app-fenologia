package com.example.hassim.fenologia;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hassim.fenologia.spinner.EtapaFenologica;

import java.util.ArrayList;

public class FormDialogCustomAdapter extends ArrayAdapter<EtapaFenologica> {

    private static final String TAG = FormDialogCustomAdapter.class.getSimpleName();
    private ArrayList<EtapaFenologica> etapaFenologicas;
    private ArrayList<String> idsFenologiaChecked = new ArrayList<>();


    public FormDialogCustomAdapter(Context context, ArrayList<EtapaFenologica> etapaFenologicas) {
        super(context, 0, etapaFenologicas);
        this.etapaFenologicas = etapaFenologicas;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.spinner_item, parent, false);
        }

        final CheckedTextView etapa = convertView.findViewById(R.id.checkedTextView1);

        etapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etapa.setChecked(!etapa.isChecked());

                if (etapa.isChecked()) {
                    idsFenologiaChecked.add(String.valueOf(etapaFenologicas.get(position).getIdFenologia()) + "|" + String.valueOf(etapaFenologicas.get(position).getEtapa()));
                } else {
                    idsFenologiaChecked.remove(String.valueOf(etapaFenologicas.get(position).getIdFenologia()) + "|" + String.valueOf(etapaFenologicas.get(position).getEtapa()));
                }

                Log.i(TAG, "Is checked: " + etapa.isChecked());
            }
        });

        int idFenologia = etapaFenologicas.get(position).getIdFenologia();
        switch (idFenologia) {
            case 1:
            case 2:
                etapa.setTextColor(convertView.getResources().getColor(R.color.one_two, null));
                break;
            case 3:
            case 4:
                etapa.setTextColor(convertView.getResources().getColor(R.color.three_four, null));
                break;
            case 5:
                etapa.setTextColor(convertView.getResources().getColor(R.color.five, null));
                break;
            case 6:
            case 7:
                etapa.setTextColor(convertView.getResources().getColor(R.color.six_seven, null));
                break;
            case 8:
            case 9:
                etapa.setTextColor(convertView.getResources().getColor(R.color.eight_nine, null));
                break;
            case 10:
                etapa.setTextColor(convertView.getResources().getColor(R.color.ten, null));
                break;
            default:
                break;
        }

        etapa.setText(etapaFenologicas.get(position).getEtapa());
        return convertView;
    }




    /* ----------------------------------------------------------------------------------------- */

    public ArrayList<String> getAllChecked() {
        return idsFenologiaChecked;
    }


}
