package com.example.hassim.fenologia.form;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.hassim.fenologia.R;

import java.util.ArrayList;
import java.util.Collections;

public class SpinnerEstadoAdapter extends ArrayAdapter<Estado> {

    private ArrayList<Estado> estados = new ArrayList<>();

    public SpinnerEstadoAdapter(@NonNull Context context, ArrayList<Estado> estados) {
        super(context, 0, estados);
        this.estados = estados;
    }

    @Override
    public boolean isEnabled(int position) {
        return position != 0;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.form_spinner_estado, parent, false);
        }
        TextView textView = convertView.findViewById(R.id.text_view_spinner_estado);
        textView.setText(estados.get(position).getNombreEstado());
        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.form_spinner_estado, parent, false);
        }
        TextView textView = convertView.findViewById(R.id.text_view_spinner_estado);
        textView.setText(estados.get(position).getNombreEstado());

        if (position == 0) {
            //textView.setTextColor(Color.GRAY);
            textView.setVisibility(View.GONE);
        } else {
            textView.setTextColor(Color.GRAY);
            textView.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return estados.size();
    }


}
