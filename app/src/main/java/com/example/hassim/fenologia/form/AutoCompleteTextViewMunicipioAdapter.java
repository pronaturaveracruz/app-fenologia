package com.example.hassim.fenologia.form;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.example.hassim.fenologia.R;

import java.util.ArrayList;
import java.util.List;

public class AutoCompleteTextViewMunicipioAdapter extends ArrayAdapter<Municipio> {

    private List<Municipio> municipios;
    private List<Municipio> filteredMunicipios;

    public AutoCompleteTextViewMunicipioAdapter(@NonNull Context context, ArrayList<Municipio> municipios) {
        super(context, 0, municipios);
        this.municipios = new ArrayList<>(municipios);
        this.filteredMunicipios = new ArrayList<>(municipios);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.auto_complete_text_view_municipio, parent, false);
        }
        //Current item
        Municipio currentMunicipio = getItem(position);

        //References
        TextView autoCompleteTextViewMunicipio = convertView.findViewById(R.id.auto_complete_text_view_municipio);
        //Behaviour
        autoCompleteTextViewMunicipio.setText(currentMunicipio != null ? currentMunicipio.getNombreMunicipio() : null);
        return convertView;
    }

    @Nullable
    @Override
    public Municipio getItem(int position) {
        return municipios.get(position);
    }

    @Override
    public int getCount() {
        return municipios.size();
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            public CharSequence convertResultToString(Object resultValue) {
                return ((Municipio) resultValue).getNombreMunicipio();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                List<Municipio> municipiosSuggestion = new ArrayList<>();
                if (constraint != null) {
                    for (Municipio municipio : filteredMunicipios) {
                        if (municipio.getNombreMunicipio().toLowerCase().contains(constraint.toString().toLowerCase())) {
                            municipiosSuggestion.add(municipio);
                        }
                    }
                    filterResults.values = municipiosSuggestion;
                    filterResults.count = municipiosSuggestion.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                municipios.clear();
                if (results != null && results.count > 0) {
                    for (Object object : (List<?>) results.values) {
                        if (object instanceof Municipio) {
                            municipios.add((Municipio) object);
                        }
                    }
                    notifyDataSetChanged();
                } else if (constraint == null) {
                    // no filter, add entire original list back in
                    municipios.addAll(filteredMunicipios);
                    notifyDataSetInvalidated();
                }
            }
        };
    }


}
