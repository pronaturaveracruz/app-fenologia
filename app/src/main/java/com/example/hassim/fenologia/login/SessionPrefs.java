package com.example.hassim.fenologia.login;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

public class SessionPrefs {

    private static final String PREFS_NAME = "FENOLOGIA_PREFS";
    private static final String PREF_USER_ID = "PREF_USER_ID";
    private static final String PREF_USER_NAME = "PREF_USER_NAME";
    private static final String PREF_USER_EMAIL = "PREF_USER_EMAIL";

    private final SharedPreferences mPrefs;
    public static boolean mIsLoggedIn;
    private static SessionPrefs INSTANCE;
    private int mCurrentUserId;

    public static SessionPrefs get(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new SessionPrefs(context);
        }
        return INSTANCE;
    }

    private SessionPrefs(Context context) {
        mPrefs = context.getApplicationContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        mIsLoggedIn = !TextUtils.isEmpty(mPrefs.getString(PREF_USER_NAME, null));
    }

    public boolean isLoggedIn() {
        return mIsLoggedIn;
    }

    public void saveUser(UserResponseModel userResponseModel) {
        if (userResponseModel != null) {
            SharedPreferences.Editor editor = mPrefs.edit();
            editor.putInt(PREF_USER_ID, userResponseModel.getUserId());
            editor.putString(PREF_USER_NAME, userResponseModel.getName());
            editor.putString(PREF_USER_EMAIL, userResponseModel.getEmail());
            //editor.apply(); In background
            editor.commit(); //Immediately
            mIsLoggedIn = true;
        }
    }

    public void logOut() {
        mIsLoggedIn = false;
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putInt(PREF_USER_ID, 0);
        editor.putString(PREF_USER_NAME, null);
        editor.putString(PREF_USER_EMAIL, null);
        //editor.apply();
        editor.commit();
    }
}
