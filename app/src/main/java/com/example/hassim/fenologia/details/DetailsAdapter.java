package com.example.hassim.fenologia.details;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.hassim.fenologia.R;

import java.util.ArrayList;

public class DetailsAdapter extends ArrayAdapter<EspecieFenologia> {

    private ArrayList<EspecieFenologia> especieFenologias;

    public DetailsAdapter(@NonNull Context context, ArrayList<EspecieFenologia> especieFenologias) {
        super(context, 0, especieFenologias);
        this.especieFenologias = especieFenologias;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_details, parent, false);
        }

        TextView nombreCientificoTextView = convertView.findViewById(R.id.text_view_nombre_cientifico_details);
        TextView nombreComunTextView = convertView.findViewById(R.id.text_view_nombre_comun_details);
        TextView etapaFenologicaTextView = convertView.findViewById(R.id.text_view_etapa_fenologica_details);

        nombreCientificoTextView.setText(especieFenologias.get(position).getNombreCientifico());
        nombreComunTextView.setText(especieFenologias.get(position).getNombreComun());

        //TODO: Change color to 'etapas fenologicas'
        String etapaFenologica = especieFenologias.get(position).getEtapaFenologica();
        switch (etapaFenologica) {
            case "Flor en botón":
            case "Solo flor":
                etapaFenologicaTextView.setTextColor(convertView.getResources().getColor(R.color.one_two, null));
                break;
            case "Fruto nuevo":
            case "Fruto en crecimiento":
                etapaFenologicaTextView.setTextColor(convertView.getResources().getColor(R.color.three_four, null));
                break;
            case "Fruto maduro":
                etapaFenologicaTextView.setTextColor(convertView.getResources().getColor(R.color.five, null));
                break;
            case "Con hojas":
            case "Brote de hojas":
                etapaFenologicaTextView.setTextColor(convertView.getResources().getColor(R.color.six_seven, null));
                break;
            case "Tirando hojas":
            case "Sin hojas":
                etapaFenologicaTextView.setTextColor(convertView.getResources().getColor(R.color.eight_nine, null));
                break;
            case "Dispersada":
                etapaFenologicaTextView.setTextColor(convertView.getResources().getColor(R.color.ten, null));
                break;
            default:
                break;
        }
        etapaFenologicaTextView.setText(especieFenologias.get(position).getEtapaFenologica());

        return convertView;

    }
}
