package com.example.hassim.fenologia.login;

import com.google.gson.annotations.SerializedName;

public class UserResponseModel {

    @SerializedName("success")
    private int status;
    @SerializedName("user_id")
    private int userID;
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("message")
    private String message;

    public UserResponseModel(int status, int userID, String name, String email, String message) {
        this.status = status;
        this.userID = userID;
        this.name = name;
        this.email = email;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public int getUserId() {
        return userID;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getMessage() {
        return message;
    }
}
