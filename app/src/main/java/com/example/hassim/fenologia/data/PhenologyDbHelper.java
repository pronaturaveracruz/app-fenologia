package com.example.hassim.fenologia.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class PhenologyDbHelper extends SQLiteOpenHelper {
    private static final String TAG = PhenologyDbHelper.class.getSimpleName();
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "phenology.db";

    private static String DATABASE_PATH;
    private SQLiteDatabase myDataBase;
    private final Context mContext;
    private SharedPreferences sharedPreferences;

    public PhenologyDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
        DATABASE_PATH = mContext.getDatabasePath(this.getDatabaseName()).getParent().concat("/");
        Log.i(TAG, DATABASE_PATH);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

  

    }

    @Override
    public synchronized void close() {
        if (myDataBase != null)
            myDataBase.close();
        super.close();
    }

    /* ----------------------------------------------------------------------------------------- */

    public void createDataBase() throws IOException {
        Log.i(TAG, "createDataBase");
        boolean dbExist = checkDataBase();
        if (!dbExist) {
            this.getReadableDatabase();
            try {
                copyDataBase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    private boolean checkDataBase() {
        try {
            String myPath = DATABASE_PATH + DATABASE_NAME;
            File f = new File(myPath);
            return f.exists();
        } catch (SQLiteException e) {
            Log.e("Podcast", "There was an error", e);
            return false;
        }
    }

    private void copyDataBase() throws IOException {
        Log.i(TAG, "copyDataBase");
        InputStream myInput = mContext.getAssets().open(DATABASE_NAME);
        String outFileName = DATABASE_PATH + DATABASE_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    public void openDataBase() throws SQLException {
        String myPath = DATABASE_PATH + DATABASE_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READWRITE);

    }


}