package com.example.hassim.fenologia.recyclerview;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.hassim.fenologia.R;
import com.example.hassim.fenologia.details.EspecieFenologia;
import com.example.hassim.fenologia.interfaces.UpdateNumEspeciesTextViewForm;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<EspecieFenologia> especieFenologias;
    private TextView mNumEspeciesTextView;
    private UpdateNumEspeciesTextViewForm updateNumEspeciesTextViewForm;
    private boolean showDeleteButton = true;

    public RecyclerViewAdapter(Context context, ArrayList<EspecieFenologia> especieFenologias) {
        mContext = context;
        this.especieFenologias = especieFenologias;
        showDeleteButton = false;
    }

    public RecyclerViewAdapter(Context context, ArrayList<EspecieFenologia> especieFenologias, UpdateNumEspeciesTextViewForm updateNumEspeciesTextViewForm) {
        mContext = context;
        this.especieFenologias = especieFenologias;
        this.updateNumEspeciesTextViewForm = updateNumEspeciesTextViewForm;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mNumEspeciesTextView = parent.findViewById(R.id.text_view_num_especies_rv_form);

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_details, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        String currentEtapaFenologica = especieFenologias.get(position).getEtapaFenologica();
        holder.mNombreCientifico.setText(especieFenologias.get(position).getNombreCientifico());
        holder.mNombreComun.setText(especieFenologias.get(position).getNombreComun());
        holder.mEtapaFenologica.setText(currentEtapaFenologica);
        //Change color to 'etapas fenologicas'
        Resources resources = holder.itemView.getContext().getResources();
        switch (currentEtapaFenologica) {
            case "Flor en botón":
            case "Solo flor":
                holder.mEtapaFenologica.setTextColor(resources.getColor(R.color.one_two, null));
                break;
            case "Fruto nuevo":
            case "Fruto en crecimiento":
                holder.mEtapaFenologica.setTextColor(resources.getColor(R.color.three_four, null));
                break;
            case "Fruto maduro":
                holder.mEtapaFenologica.setTextColor(resources.getColor(R.color.five, null));
                break;
            case "Con hojas":
            case "Brote de hojas":
                holder.mEtapaFenologica.setTextColor(resources.getColor(R.color.six_seven, null));
                break;
            case "Tirando hojas":
            case "Sin hojas":
                holder.mEtapaFenologica.setTextColor(resources.getColor(R.color.eight_nine, null));
                break;
            case "Dispersada":
                holder.mEtapaFenologica.setTextColor(resources.getColor(R.color.ten, null));
                break;
            default:
                break;
        }
        //Remove each delete button in RV if the activity is: DetailActivity
        if (!showDeleteButton) {
            holder.mButtonRemoveFromRV.setVisibility(View.GONE);
        } else {
            holder.mButtonRemoveFromRV.setVisibility(View.VISIBLE);
        }

        //Add a click listener to remove button in each row
        holder.mButtonRemoveFromRV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.i("rvAdapter", "Remove the item: #" + holder.getAdapterPosition());

                // 0) Open dialog message to confirm removal
                DialogInterface.OnClickListener discardButtonClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // 1) Remove current item from RV
                        int currentPosition = holder.getAdapterPosition();
                        Log.i("rvAdapter", "REMOVE ITEM #" + currentPosition + " FROM RV");
                        especieFenologias.remove(currentPosition);
                        notifyItemRemoved(currentPosition);
                        notifyItemRangeChanged(currentPosition, getItemCount());
                        Log.i("rvAdapter", "Numero de especies actualizada: " + getItemCount());
                        // 2) Update the title of the RV (Current items)
                        updateNumEspeciesTextViewForm.update(getItemCount(), currentPosition);


                    }
                };
                showDialogRemoveItemFromRV(mContext, discardButtonClickListener);

            }
        });


    }

    @Override
    public int getItemCount() {
        return especieFenologias.size();
    }



    /* ------------------------------------------------------------------------------------- */

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageButton mButtonRemoveFromRV;
        TextView mNombreCientifico, mNombreComun, mEtapaFenologica;

        MyViewHolder(View view) {
            super(view);
            mButtonRemoveFromRV = view.findViewById(R.id.remove_from_rv_button);
            mNombreCientifico = view.findViewById(R.id.text_view_nombre_cientifico_details);
            mNombreComun = view.findViewById(R.id.text_view_nombre_comun_details);
            mEtapaFenologica = view.findViewById(R.id.text_view_etapa_fenologica_details);
        }
    }

    private void showDialogRemoveItemFromRV(Context context, DialogInterface.OnClickListener discardButtonClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.title_dialog_remove_item_rv);
        builder.setPositiveButton(R.string.remove_item_rv, discardButtonClickListener);
        builder.setNegativeButton(R.string.cancel_remove_item_rv, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


}
