package com.example.hassim.fenologia.spinner;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hassim.fenologia.R;

import java.util.ArrayList;

public class SpinnerAdapter extends ArrayAdapter<EtapaFenologica> {

    private ArrayList<EtapaFenologica> etapaFenologicas;

    public SpinnerAdapter(Context context, ArrayList<EtapaFenologica> etapaFenologicas) {
        super(context, 0, etapaFenologicas);
        this.etapaFenologicas = etapaFenologicas;
    }

    //Disable first item from Spinner
    @Override
    public boolean isEnabled(int position) {
        return position != 0;
    }

    @Override
    public int getCount() {
        return etapaFenologicas.size();
    }

    @Override
    public EtapaFenologica getItem(int position) {
        return etapaFenologicas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.spinner_item, parent, false);
        }
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {       
        return convertView;
    }
}
