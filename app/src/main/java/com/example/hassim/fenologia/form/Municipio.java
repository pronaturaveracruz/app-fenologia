package com.example.hassim.fenologia.form;

public class Municipio {

    private String mIdMunicipio;
    private String mNombreMunicipio;

    public Municipio(String idMunicipio, String nombreMunicipio) {
        this.mIdMunicipio = idMunicipio;
        this.mNombreMunicipio = nombreMunicipio;
    }

    public String getIdMunicipio() {
        return mIdMunicipio;
    }

    public String getNombreMunicipio() {
        return mNombreMunicipio;
    }
}
