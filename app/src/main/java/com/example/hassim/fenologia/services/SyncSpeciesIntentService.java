package com.example.hassim.fenologia.services;

import android.app.Activity;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.hassim.fenologia.MainActivity;
import com.example.hassim.fenologia.R;
import com.example.hassim.fenologia.internet.ApiClient;
import com.example.hassim.fenologia.internet.ApiService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import com.example.hassim.fenologia.data.PhenologyContract.*;

public class SyncSpeciesIntentService extends IntentService {

    private static final int NOTIFICATION_ID = 1;
    private static final String CHANNEL_ID = "1";

    private static final int RETROFIT_RESULT_OK = 1;
    private static final int RETROFIT_RESULT_FAILED = 2;
    private static final int RETROFIT_RESULT_NO_CHANGES = 3;

    private ApiService mApiService;
    private ContentResolver mContentResolver;

    String TAG = "IntentService";

    public SyncSpeciesIntentService() {
        super(SyncSpeciesIntentService.class.getName());
        setIntentRedelivery(true);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        mApiService = ApiClient.getClient().create(ApiService.class);
        mContentResolver = getContentResolver();

        if (intent != null) {
            ResultReceiver rec = intent.getParcelableExtra("receiver");
            if (isOnline()) {
                sendNotification("Sincronizando...", R.drawable.ic_cloud_sync_white_48dp, true);
                int result = makeRetrofitCall();
                if (result == RETROFIT_RESULT_OK) {
                    //Return to the UI a Bundle with a message
                    Bundle bundle = new Bundle();
                    bundle.putString("resultValue", "Sincronización completa");
                    rec.send(Activity.RESULT_OK, bundle);
                    sendNotification("Sincronización completa", R.drawable.ic_check_circle_white_48dp, false);
                } else if (result == RETROFIT_RESULT_FAILED) {
                    //Return to the UI a Bundle with a message
                    Bundle bundle = new Bundle();
                    bundle.putString("resultValue", "Sincronización falló, intenta nuevamente...");
                    rec.send(Activity.RESULT_OK, bundle);
                    sendNotification("Sincronización falló, intenta nuevamente...", R.drawable.ic_alert_circle_white_48dp, false);
                } else if (result == RETROFIT_RESULT_NO_CHANGES) {
                    //Return to the UI a Bundle with a message
                    Bundle bundle = new Bundle();
                    bundle.putString("resultValue", "Ya tienes la ultima versión.");
                    rec.send(Activity.RESULT_OK, bundle);
                    sendNotification("Ya tienes la ultima versión de la lista de especies.", R.drawable.ic_check_circle_white_48dp, false);
                }
            }
        }
    }

    /* ---------------------------------------------------------------------------------- */

    private void sendNotification(String msg, int iconDrawable, boolean onGoing) {
        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setContentTitle(msg)
                        .setOngoing(onGoing)
                        .setColor(getResources().getColor(R.color.colorPrimary, null))
                        .setSmallIcon(iconDrawable);
        mBuilder.setContentIntent(contentIntent);
        if (mNotificationManager != null) {
            mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        }
    }

    private boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
        return activeNetwork != null && activeNetwork.isConnected();
    }

    private int makeRetrofitCall() {
        Log.i(TAG, "makeRetrofitCall");


        //Make the Retrofit call (Synchronously) [execute]
        // START THE SYNC ////////////////////////////////////////////////////////////////////
        Log.i(TAG, "Start the sync");
        int returnValue = RETROFIT_RESULT_FAILED;
        Cursor cursorSyncLog = mContentResolver.query(
                SyncLogEntry.CONTENT_URI,
                null,
                null,
                null,
                "_id DESC LIMIT 1"
        );
        if (cursorSyncLog != null) {
            if (cursorSyncLog.moveToFirst() && cursorSyncLog.getCount() > 0) {

                int lastSyncDateIndex = cursorSyncLog.getColumnIndex(SyncLogEntry.COLUMN_FECHA);
                int currentRowsInEspeciesSQLiteSyncLogTableIndex = cursorSyncLog.getColumnIndex(SyncLogEntry.COLUMN_TOTAL_ESPECIES);
                String lastSyncDateSyncLogTable = cursorSyncLog.getString(lastSyncDateIndex);
                int currentRowsInEspeciesSQLiteSyncLogTable = cursorSyncLog.getInt(currentRowsInEspeciesSQLiteSyncLogTableIndex);

                Log.i(TAG, "lastSyncDateSyncLogTable: " + lastSyncDateSyncLogTable + " currentRowsInEspeciesSQLiteSyncLogTable: " + currentRowsInEspeciesSQLiteSyncLogTable);

                //EXPERIMENTAL
                Call<SyncSpeciesResponseModel> call = mApiService.syncTableSpecies(lastSyncDateSyncLogTable, currentRowsInEspeciesSQLiteSyncLogTable);
                try {
                    Response response = call.execute();
                    //Log.i("IntentService", response.toString());
                    if (response.isSuccessful()) {
                        Log.i(TAG, "Is successful :)");
                        SyncSpeciesResponseModel syncSpeciesResponseModel = (SyncSpeciesResponseModel) response.body();
                        if (syncSpeciesResponseModel != null) {
                            if (syncSpeciesResponseModel.getStatus() == 1) {

                                //1) Handle 'string_to_insert'
                                int totalToInsert = 0;
                                int counterInsertsOK = 0;
                                String stringToInsert = syncSpeciesResponseModel.getStringToInsert();
                                if (stringToInsert != null) {
                                    Log.i(TAG, "INSERT");
                                    logLongString(stringToInsert);
                                    String[] rowsToInsert = stringToInsert.split("\\|");
                                    totalToInsert = rowsToInsert.length;
                                    if (totalToInsert > 0) {
                                        String[] singleItems;
                                        Uri insertUri;
                                        for (String rowToInsert : rowsToInsert) {
                                            singleItems = rowToInsert.split(",");
                                            if (singleItems.length == 3) {
                                                //Validate if this data is already in the database.
                                                Cursor cursor = mContentResolver.query(
                                                        EspeciesEntry.CONTENT_URI,
                                                        null,
                                                        //"id_num ='" + singleItems[0] + "' AND nombre_cientifico = '" + singleItems[1] + "' AND nombre_comun = '" + singleItems[2] + "'",
                                                        " id_num ='" + singleItems[0] + "'",
                                                        null,
                                                        null
                                                );
                                                if (cursor != null) {
                                                    if (!(cursor.moveToFirst()) || cursor.getCount() == 0) {
                                                        //cursor is empty
                                                        Log.i(TAG, "INSERT NEW WITH: id_num = " + singleItems[0] + " / nombre_cientifico = " + singleItems[1] + " / nombre_comun = " + singleItems[2]);
                                                        ContentValues contentValuesInsert = new ContentValues();
                                                        contentValuesInsert.put(EspeciesEntry.COLUMN_ID_NUM, singleItems[0]);
                                                        contentValuesInsert.put(EspeciesEntry.COLUMN_NOMBRE_CIENTIFICO, singleItems[1]);
                                                        contentValuesInsert.put(EspeciesEntry.COLUMN_NOMBRE_COMUN, singleItems[2]);
                                                        insertUri = mContentResolver.insert(EspeciesEntry.CONTENT_URI, contentValuesInsert);
                                                        if (insertUri != null) {
                                                            counterInsertsOK++;
                                                        }
                                                    } else if (cursor.moveToFirst() && cursor.getCount() > 0) {
                                                        Log.i(TAG, "This row is already in SQLite, skipping... (id_num = " + singleItems[0] + ")");
                                                        counterInsertsOK++;
                                                    }
                                                    if (!cursor.isClosed()) {
                                                        cursor.close();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                //TODO: 2) Handle 'string_to_update'
                                int totalToUpdate = 0;
                                int counterUpdatesOK = 0;
                                String stringToUpdate = syncSpeciesResponseModel.getStringToUpdate();
                                if (stringToUpdate != null) {
                                    Log.i(TAG, "UPDATE");
                                    logLongString(stringToUpdate);
                                    String[] rowsToUpdate = stringToUpdate.split("\\|");
                                    totalToUpdate = rowsToUpdate.length;
                                    if (totalToUpdate > 0) {
                                        String[] singleItems;
                                        for (String rowToUpdate : rowsToUpdate) {
                                            singleItems = rowToUpdate.split(",");
                                            if (singleItems.length == 3) {
                                                Log.i(TAG, "UPDATE WITH: id_num = " + singleItems[0] + " / nombre_cientifico = " + singleItems[1] + " / nombre_comun_esp = " + singleItems[2]);
                                                ContentValues contentValuesUpdate = new ContentValues();
                                                contentValuesUpdate.put(EspeciesEntry.COLUMN_NOMBRE_CIENTIFICO, singleItems[1]);
                                                contentValuesUpdate.put(EspeciesEntry.COLUMN_NOMBRE_COMUN, singleItems[2]);
                                                int rowsUpdated = mContentResolver.update(EspeciesEntry.CONTENT_URI, contentValuesUpdate, EspeciesEntry.COLUMN_ID_NUM + "=?", new String[]{singleItems[0]});
                                                if (rowsUpdated > 0) {
                                                    counterUpdatesOK++;
                                                }
                                            }
                                        }
                                    }
                                }


                                if ((totalToInsert == counterInsertsOK && counterInsertsOK > 0) || (totalToUpdate == counterUpdatesOK && counterUpdatesOK > 0)) {
                                    Log.i(TAG, "Insert in sync_log please!! :D");
                                    String dateFromServer = syncSpeciesResponseModel.getDateFromServer();
                                    int totalRowsFromServer = syncSpeciesResponseModel.getTotalRowsServer();
                                    Log.i(TAG, "With data: " + dateFromServer + " - " + totalRowsFromServer);
                                    ContentValues contentValuesInsertSyncLog = new ContentValues();
                                    contentValuesInsertSyncLog.put(SyncLogEntry.COLUMN_FECHA, dateFromServer);
                                    contentValuesInsertSyncLog.put(SyncLogEntry.COLUMN_TOTAL_ESPECIES, totalRowsFromServer);
                                    Uri insertSyncLogUri = mContentResolver.insert(SyncLogEntry.CONTENT_URI, contentValuesInsertSyncLog);
                                    if (insertSyncLogUri != null) {
                                        Log.i(TAG, "RETROFIT_RESULT_OK");
                                        returnValue = RETROFIT_RESULT_OK;
                                    }
                                }


                            } else {
                                //PHP returns status = 0 (Nothing changed)
                                Log.i(TAG, syncSpeciesResponseModel.getMessage());
                                Log.i(TAG, "RETROFIT_RESULT_NO_CHANGES");
                                returnValue = RETROFIT_RESULT_NO_CHANGES;
                            }
                        }
                    } else {
                        //Error in the response
                        Log.i(TAG, "Not successful :(");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //TODO: END OF EXPERIMENTAL


            } else {
                Log.i(TAG, "The table 'sync_log' is empty");
            }

            if (!cursorSyncLog.isClosed()) {
                cursorSyncLog.close();
            }
        }


        /////////////////////////////////////////////////////////////////////////////////////

        //return RETROFIT_RESULT_FAILED;
        return returnValue;
    }

    /* ----------------------------------------------------------------------------------------- */
    private void logLongString(String someString) {
        List<String> strings = new ArrayList<>();
        int index = 0;
        while (index < someString.length()) {
            strings.add(someString.substring(index, Math.min(index + 500, someString.length())));
            index += 500;
        }
        for (int i = 0; i < strings.size(); i++) {
            Log.i(TAG, "Fragment #" + i + ": " + strings.get(i));
        }
    }

}
