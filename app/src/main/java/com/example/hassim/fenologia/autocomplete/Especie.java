package com.example.hassim.fenologia.autocomplete;

public class Especie {

    private int id_num;
    private String nombre_cientifico;
    private String nombre_comun;

    public Especie(int id_num, String nombre_cientifico, String nombre_comun) {
        this.id_num = id_num;
        this.nombre_cientifico = nombre_cientifico;
        this.nombre_comun = nombre_comun;
    }

    public int getIdNum() {
        return id_num;
    }

    public String getNombreCientifico() {
        return nombre_cientifico;
    }

    public String getNombreComun() {
        return nombre_comun;
    }
}
