package com.example.hassim.fenologia.form;

public class Estado {

    private String mIdEstado;
    private String mNombreEstado;

    public Estado(String mIdEstado, String mNombreEstado) {
        this.mIdEstado = mIdEstado;
        this.mNombreEstado = mNombreEstado;
    }

    public String getIdEstado() {
        return mIdEstado;
    }

    public String getNombreEstado() {
        return mNombreEstado;
    }
}
