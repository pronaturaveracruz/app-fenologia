package com.example.hassim.fenologia.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class PhenologyContract {
    public static final String CONTENT_AUTHORITY = "com.example.hassim.fenologia";
    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_PRINCIPAL = "sisresem_cf_principal";
    public static final String PATH_PRINCIPAL_REGISTROS = "principal_registros";
    public static final String PATH_REGISTROS = "sisresem_cf_registro";
    public static final String PATH_FENOLOGIA = "sisresem_cf_fenologia";
    public static final String PATH_ESPECIES = "especies";
    public static final String PATH_UBICACION = "ubicacion";
    public static final String PATH_SYNC_LOG = "sisresem_sync_log";

    public static final int STATUS_NOT_SYNC = 0;
    public static final int STATUS_SYNC_OK = 1;
    public static final int STATUS_ERROR = 2;

    public static abstract class PrincipalEntry implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_PRINCIPAL);
        public static final String CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PRINCIPAL;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PRINCIPAL;
        public static final Uri CONTENT_URI_WITH_JOIN = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_PRINCIPAL_REGISTROS);
        public static final String CONTENT_LIST_TYPE2 = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PRINCIPAL_REGISTROS;
        public static final String CONTENT_ITEM_TYPE2 = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PRINCIPAL_REGISTROS;
        public static final String TABLE_NAME = "sisresem_cf_principal";
        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_ID_PRINCIPAL = "id_principal";
        public static final String COLUMN_FECHA = "fecha";
        public static final String COLUMN_LONGITUD_GMS = "longitud_gms";
        public static final String COLUMN_LATITUD_GMS = "latitud_gms";
        public static final String COLUMN_LONGITUD = "longitud_dec";
        public static final String COLUMN_LATITUD = "latitud_dec";
        public static final String COLUMN_CVE_ESTADO = "cve_estado";
        public static final String COLUMN_CVE_MUNICIPIO = "cve_municipio";
        public static final String COLUM_ID_USUARIO = "id_usuario";
        public static final String COLUMN_SYNC_STATUS = "sync_status";
        public static final String COLUMN_NUM_ESPECIES = "num_especies";
        public static final String COLUMN_ESPECIES_CONCAT = "especies_concat";
        public static final String COLUMN_ESPECIES_TEXT_CONCAT = "especies_text_concat";
    }

    public static abstract class RegistroEntry implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_REGISTROS);
        public static final String CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_REGISTROS;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_REGISTROS;
        public static final String TABLE_NAME = "sisresem_cf_registro";
        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_ID_REGISTRO = "id_registro";
        public static final String COLUMN_ID_PRINCIPAL = "id_principal";
        public static final String COLUMN_ID_ESPECIE = "id_especie";
        public static final String COLUMN_ID_FENOLOGIA = "id_fenologia";
    }

    public static abstract class FenologiaEntry implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_FENOLOGIA);
        public static final String CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_FENOLOGIA;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_FENOLOGIA;
        public static final String TABLE_NAME = "sisresem_cf_fenologia";
        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_ID_FENOLOGIA = "id_fenologia";
        public static final String COLUMN_LETRA = "letra";
        public static final String COLUMN_ETAPA = "etapa";
    }

    public static abstract class EspeciesEntry implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_ESPECIES);
        public static final String CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_ESPECIES;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_ESPECIES;
        public static final String TABLE_NAME = "especies";
        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_ID_NUM = "id_num";
        public static final String COLUMN_NOMBRE_CIENTIFICO = "nombre_cientifico";
        public static final String COLUMN_NOMBRE_COMUN = "nombre_comun";
    }

    public static abstract class UbicacionEntry implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_UBICACION);
        public static final String CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_UBICACION;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_UBICACION;
        public static final String TABLE_NAME = "ubicacion";
        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_ID_UBICACION = "id_ubicacion";
        public static final String COLUMN_ID_ESTADO = "id_estado";
        public static final String COLUMN_NOM_ESTADO = "nom_estado";
        public static final String COLUMN_ID_MUNICIPIO = "id_municipio";
        public static final String COLUMN_NOM_MUNICIPIO = "nom_municipio";
    }

    public static abstract class SyncLogEntry implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_SYNC_LOG);
        public static final String CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SYNC_LOG;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SYNC_LOG;
        public static final String TABLE_NAME = "sisresem_sync_log";
        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_FECHA = "fecha";
        public static final String COLUMN_TOTAL_ESPECIES = "total_especies";
    }

}
