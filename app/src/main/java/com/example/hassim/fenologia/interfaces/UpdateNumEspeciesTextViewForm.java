package com.example.hassim.fenologia.interfaces;

public interface UpdateNumEspeciesTextViewForm {

    void update(int itemsCount, int itemRemoved);

}
