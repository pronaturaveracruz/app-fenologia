package com.example.hassim.fenologia.internet;

import com.google.gson.annotations.SerializedName;

public class InsertPrincipalResponseModel {
    @SerializedName("success")
    private int status;
    @SerializedName("inserted_id")
    private int insertedId;
    @SerializedName("string_update_sqlite")
    private String stringUpdateSQLite;
    @SerializedName("sync_status")
    private int syncStatus;
    @SerializedName("message")
    private String message;

    public InsertPrincipalResponseModel(int status, int insertedId, String stringUpdateSQLite, int syncStatus, String message) {
        this.status = status;
        this.insertedId = insertedId;
        this.stringUpdateSQLite = stringUpdateSQLite;
        this.syncStatus = syncStatus;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public int getInsertedId() {
        return insertedId;
    }

    public String getStringUpdateSQLite() {
        return stringUpdateSQLite;
    }

    public int getSyncStatus() {
        return syncStatus;
    }

    public String getMessage() {
        return message;
    }

}
