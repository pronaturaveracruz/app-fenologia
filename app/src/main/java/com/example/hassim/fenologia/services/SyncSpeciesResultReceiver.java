package com.example.hassim.fenologia.services;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class SyncSpeciesResultReceiver extends ResultReceiver {

    private Receiver receiver;

    public SyncSpeciesResultReceiver(Handler handler) {
        super(handler);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (receiver != null) {
            receiver.onReceiveResult(resultCode, resultData);
        }
    }



    /* ------------------------------------------------------------------------------------- */

    // Setter for assigning the receiver
    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    // Defines our event interface for communication
    public interface Receiver {
        void onReceiveResult(int resultCode, Bundle resultData);
    }


}
