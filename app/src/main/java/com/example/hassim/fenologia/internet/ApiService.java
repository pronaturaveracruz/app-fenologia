package com.example.hassim.fenologia.internet;

import com.example.hassim.fenologia.login.UserResponseModel;
import com.example.hassim.fenologia.services.SyncSpeciesResponseModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiService {

    @FormUrlEncoded
    @POST("insert_auto_mode.php")
    Call<InsertPrincipalResponseModel> insertInAutoMode(
            @Field("fecha") String fecha,
            @Field("latitud") String latitud,
            @Field("longitud") String longitud,
            @Field("especies_concat") String especies_concat,
            @Field("id_usuario") int id_usuario
    );

    @FormUrlEncoded
    @POST("insert_manual_mode.php")
    Call<InsertPrincipalResponseModel> insertInManualMode(
            @Field("fecha") String fecha,
            @Field("latitud_dec") String latitud_dec,
            @Field("longitud_dec") String longitud_dec,
            @Field("latitud_gms") String latitud_gms,
            @Field("longitud_gms") String longitud_gms,
            @Field("cve_estado") String cve_estado,
            @Field("cve_municipio") String cve_municipio,
            @Field("especies_concat") String especies_concat,
            @Field("id_usuario") int id_usuario
    );

    @FormUrlEncoded
    @POST("verify_if_error_rows_are_ok.php")
    Call<InsertPrincipalResponseModel> verifyIfErrorRowsAreOk(
            @Field("remote_id") long remoteID
    );

    @FormUrlEncoded
    @POST("login.php")
    Call<UserResponseModel> login(
            @Field("form_user") String form_user,
            @Field("form_pass") String form_pass
    );

    @FormUrlEncoded
    @POST("sync_table_species.php")
    Call<SyncSpeciesResponseModel> syncTableSpecies(
            @Field("last_sync_datetime_sqlite") String lastSyncDateTimeSQLite,
            @Field("last_species_count_sqlite") int lastSpeciesCountSQLite
    );

}
