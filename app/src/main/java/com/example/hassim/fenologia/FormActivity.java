package com.example.hassim.fenologia;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hassim.fenologia.autocomplete.AutoCompleteAdapter;
import com.example.hassim.fenologia.autocomplete.Especie;

import com.example.hassim.fenologia.details.EspecieFenologia;
import com.example.hassim.fenologia.form.AutoCompleteTextViewMunicipioAdapter;
import com.example.hassim.fenologia.form.Estado;
import com.example.hassim.fenologia.form.Municipio;
import com.example.hassim.fenologia.form.SpinnerEstadoAdapter;
import com.example.hassim.fenologia.interfaces.UpdateNumEspeciesTextViewForm;
import com.example.hassim.fenologia.recyclerview.RecyclerViewAdapter;
import com.example.hassim.fenologia.spinner.EtapaFenologica;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import com.example.hassim.fenologia.data.PhenologyContract.*;

public class FormActivity extends AppCompatActivity implements UpdateNumEspeciesTextViewForm {

    private static final String TAG = FormActivity.class.getSimpleName();
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    public static final int REQUEST_CHECK_SETTINGS = 2;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    protected Location mCurrentLocation;
    private Boolean mRequestingLocationUpdates;
    EditText mDateEditText, mLatitudeEditText, mLongitudeEditText;
    TextInputLayout mDateTIL;
    AutoCompleteTextView mSpeciesAutoCompleteTextView;
    TextView mTextViewSelectedSpecies;
    private View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mFormHasChanged = true;
        }
    };
    AutoCompleteAdapter autoCompleteAdapter;
    private boolean mFormHasChanged = false;

    int currentIdEspecie = 0;
    String currentSelectedSpecies;

    String currentTextNombreCientifico, currentTextNombreComun;


    RecyclerView mRecyclerViewForm;
    RecyclerView.LayoutManager mLayoutManager;

    ArrayList<EspecieFenologia> especieFenologias;
    RecyclerViewAdapter recyclerViewAdapter;

    View mParentLinearLayoutForm;
    ProgressBar mProgressBar;
    TextView mProgressBarTextView;

    TextView mNumEspeciesRvFormTextView;


    View mLinearLayoutAutoMode, mLinearLayoutManualMode;
    int currentUserId;
    private boolean mIsAutoModeEnabled;
    EditText mLatitudeDegreesEditText, mLatitudeMinutesEditText, mLatitudeSecondsEditText, mLongitudeDegreesEditText, mLongitudeMinutesEditText, mLongitudeSecondsEditText;
    Spinner spinnerEstado;
    boolean isSpinnerEstadoSelected, isAutoCompleteTextViewMunicipioSelected, isAutoCompleteTextViewEspecieSelected;
    AutoCompleteTextView mAutoCompleteTextViewMunicipio;
    private Toast mToast;

    //Global
    String selectedIdEstado, currentIdMunicipio;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        //Get the logged User ID
        SharedPreferences myShared = getSharedPreferences("FENOLOGIA_PREFS", MODE_PRIVATE);
        currentUserId = myShared.getInt("PREF_USER_ID", 99);
        Log.i(TAG, "Current ID Usuario: " + currentUserId);

        //References
        mDateTIL = findViewById(R.id.text_input_layout_date);
        mDateEditText = findViewById(R.id.edit_date);

        mLatitudeEditText = findViewById(R.id.edit_latitude);
        mLongitudeEditText = findViewById(R.id.edit_longitude);

        mLatitudeDegreesEditText = findViewById(R.id.edit_text_latitud_grados);
        mLatitudeMinutesEditText = findViewById(R.id.edit_text_latitud_minutos);
        mLatitudeSecondsEditText = findViewById(R.id.edit_text_latitud_segundos);
        mLongitudeDegreesEditText = findViewById(R.id.edit_text_longitud_grados);
        mLongitudeMinutesEditText = findViewById(R.id.edit_text_longitud_minutos);
        mLongitudeSecondsEditText = findViewById(R.id.edit_text_longitud_segundos);
        spinnerEstado = findViewById(R.id.spinner_estado);
        mAutoCompleteTextViewMunicipio = findViewById(R.id.auto_complete_text_view_municipio);

        mLinearLayoutAutoMode = findViewById(R.id.linear_layout_auto_mode);
        mLinearLayoutManualMode = findViewById(R.id.linear_layout_manual_mode);

        // MANUAL COORDS
        final ArrayList<Estado> estados = new ArrayList<>();
        estados.add(new Estado("0", "Selecciona una opción..."));
        String[] customProjection = {"id_estado, nom_estado"};
        String orderBy = "nom_estado";
        final Cursor cursor = getContentResolver().query(UbicacionEntry.CONTENT_URI, customProjection, null, null, orderBy);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                estados.add(new Estado(cursor.getString(cursor.getColumnIndex(UbicacionEntry.COLUMN_ID_ESTADO)), cursor.getString(cursor.getColumnIndex(UbicacionEntry.COLUMN_NOM_ESTADO))));
            } while (cursor.moveToNext());
            cursor.close();
        }
        SpinnerEstadoAdapter spinnerEstadoAdapter = new SpinnerEstadoAdapter(this, estados);
        spinnerEstado.setAdapter(spinnerEstadoAdapter);
        spinnerEstado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedIdEstado = estados.get(position).getIdEstado();
                Log.i(TAG, "selectedIdEstado: " + selectedIdEstado);
                if (!selectedIdEstado.equals("0")) {
                    isSpinnerEstadoSelected = true;
                    ArrayList<Municipio> municipios = new ArrayList<>();
                    String[] customProjection = {"id_municipio, nom_municipio"};
                    String selection = UbicacionEntry.COLUMN_ID_ESTADO + "=?";
                    //String[] selectionArgs = {"30"};
                    String[] selectionArgs = {selectedIdEstado};
                    String orderBy = "nom_municipio";
                    Cursor cursorMunicipio = getContentResolver().query(UbicacionEntry.CONTENT_URI, customProjection, selection, selectionArgs, orderBy);
                    if (cursorMunicipio != null && cursorMunicipio.moveToFirst()) {
                        do {
                            municipios.add(new Municipio(cursorMunicipio.getString(cursorMunicipio.getColumnIndex(UbicacionEntry.COLUMN_ID_MUNICIPIO)), cursorMunicipio.getString(cursorMunicipio.getColumnIndex(UbicacionEntry.COLUMN_NOM_MUNICIPIO))));
                        } while (cursorMunicipio.moveToNext());
                        cursorMunicipio.close();
                    }
                    final AutoCompleteTextViewMunicipioAdapter autoCompleteTextViewMunicipioAdapter = new AutoCompleteTextViewMunicipioAdapter(getApplicationContext(), municipios);
                    mAutoCompleteTextViewMunicipio.setAdapter(autoCompleteTextViewMunicipioAdapter);
                    mAutoCompleteTextViewMunicipio.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Municipio currentMunicipio = autoCompleteTextViewMunicipioAdapter.getItem(position);
                            if (currentMunicipio != null) {
                                isAutoCompleteTextViewMunicipioSelected = true;
                                currentIdMunicipio = currentMunicipio.getIdMunicipio();
                                String currentNombreMunicipio = currentMunicipio.getNombreMunicipio();
                                Log.i(TAG, "Your current selection: " + currentIdMunicipio + " - " + currentNombreMunicipio);
                            }
                        }
                    });
                    mAutoCompleteTextViewMunicipio.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            isAutoCompleteTextViewMunicipioSelected = false;
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                    mAutoCompleteTextViewMunicipio.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        mSpeciesAutoCompleteTextView = findViewById(R.id.auto_complete_text_view_species);
        mTextViewSelectedSpecies = findViewById(R.id.text_view_selected_species);
        mNumEspeciesRvFormTextView = findViewById(R.id.text_view_num_especies_rv_form);

        especieFenologias = new ArrayList<>();
        mProgressBar = findViewById(R.id.progress_bar);
        mProgressBarTextView = findViewById(R.id.progress_bar_text_view);
        mParentLinearLayoutForm = findViewById(R.id.parent_linear_layout_form);

        //Change species list from ListView to RecyclerView
        mRecyclerViewForm = findViewById(R.id.recycler_view_form);
        mNumEspeciesRvFormTextView.setText(getString(R.string.placeholder_numespecies_rv, 0));
        mRecyclerViewForm.setVisibility(View.GONE);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerViewForm.setLayoutManager(mLayoutManager);
        recyclerViewAdapter = new RecyclerViewAdapter(this, especieFenologias, this);
        mRecyclerViewForm.setAdapter(recyclerViewAdapter);

        //Setup AutoCompleteView
        setupAutoCompleteEspecies();

        //Get current date
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); //DEPRECATED
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        String currentDate = sdf.format(new Date());
        mDateEditText.setText(currentDate);

        //Flag
        mRequestingLocationUpdates = false;

        //Setup fusedLocation
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        //Process the LocationUpdates
        createLocationCallback();
        createLocationRequest();
        buildLocationSettingsRequest();

        //Setup the onClickListeners
        mLatitudeEditText.setOnClickListener(mClickListener);
        mLongitudeEditText.setOnClickListener(mClickListener);

    }

    @Override
    protected void onStart() {
        super.onStart();
        startLocationPermissionRequest();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
        if (checkPermissions()) {
            startLocationUpdates();
        }
        updateUI();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
        stopLocationUpdates();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_form, menu);
        // New listener to the Save button
        final MenuItem signItem = menu.findItem(R.id.action_save);
        signItem.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOptionsItemSelected(signItem);
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                savePhenology();
                return true;
            case android.R.id.home:
                if (!mFormHasChanged) {
                    NavUtils.navigateUpFromSameTask(FormActivity.this);
                    return true;
                }
                DialogInterface.OnClickListener discardButtonClickListener =
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                NavUtils.navigateUpFromSameTask(FormActivity.this);
                            }
                        };
                showUnsavedChangesDialog(discardButtonClickListener);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!mFormHasChanged) {
            super.onBackPressed();
            return;
        }
        DialogInterface.OnClickListener discardButtonClickListener =
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                };
        showUnsavedChangesDialog(discardButtonClickListener);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "Permission granted");
                startLocationUpdates();
            } else {
                Log.i(TAG, "Permission denied");
                requestPermissions();
            }
        }
    }


    /* FUNCTIONS */

    private void startLocationUpdates() {
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                        //EXPERIMENTAL: STOP THE LOCATION UPDATES ON PAUSE
                        mRequestingLocationUpdates = true;
                        //END OF EXPERIMENTAL
                        activateAutoMode();
                        updateUI();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " + "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(FormActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "No se pudo obtener tu ubicación, captura los datos manualmente";
                                Log.e(TAG, errorMessage);
                                mRequestingLocationUpdates = false;
                        }
                        // Hide the progress bar
                        mProgressBar.setVisibility(View.GONE);
                        mProgressBarTextView.setVisibility(View.GONE);
                        mParentLinearLayoutForm.setVisibility(View.VISIBLE);
                        activateManualMode();
                        updateUI();
                    }
                });
    }

    private void updateUI() {
        updateLocationUI();
    }

    private void stopLocationUpdates() {
        if (!mRequestingLocationUpdates) {
            Log.d(TAG, "stopLocationUpdates: updates never requested, no-op.");
            return;
        }
        Log.d(TAG, "removeLocationUpdates");
        mFusedLocationClient.removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mRequestingLocationUpdates = false;
                    }
                });
    }

    /* ------------------------------------------------------------------------- */

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                mCurrentLocation = locationResult.getLastLocation();
                updateLocationUI();
            }
        };
    }

    private void updateLocationUI() {
        if (mCurrentLocation != null) {
            mLatitudeEditText.setText(String.valueOf(mCurrentLocation.getLatitude()));
            mLongitudeEditText.setText(String.valueOf(mCurrentLocation.getLongitude()));
            //Show LinearLayout and hide the ProgressBar
            mProgressBar.setVisibility(View.GONE);
            mProgressBarTextView.setVisibility(View.GONE);
            mParentLinearLayoutForm.setVisibility(View.VISIBLE);
            mLatitudeEditText.setEnabled(false);
            mLongitudeEditText.setEnabled(false);
        }
    }

    private void createLocationRequest() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        //Switch accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY); //PRIORITY_HIGH_ACCURACY: GPS
    }

    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    /* -------------------------------------------------------------------------- */

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
        if (shouldProvideRationale) {
            Log.i(TAG, "User select 'deny' permission once");
            showSnackbar(R.string.permission_denied_explanation, R.string.snackbar_retry_permissions,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startLocationPermissionRequest();
                        }
                    });
        } else {
            Log.i(TAG, "User select 'Dont ask again' in the permission dialog.");
            showSnackbar(R.string.snackbar_dont_ask_again_state, android.R.string.ok,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //Do nothing for now
                            //Enable Coordinates EditTexts
                            mLatitudeEditText.setEnabled(true);
                            mLongitudeEditText.setEnabled(true);
                        }
                    });

        }
    }

    private void showSnackbar(final int mainTextStringId, final int actionStringId, View.OnClickListener listener) {
        Snackbar.make(findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(FormActivity.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    /* ---------------------------------------------------------------------------------------- */

    private void showUnsavedChangesDialog(DialogInterface.OnClickListener discardButtonClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.unsaved_changes_dialog_msg);
        builder.setPositiveButton(R.string.discard, discardButtonClickListener);
        builder.setNegativeButton(R.string.keep_editing, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /* ----------------------------------------------------------------------------------------- */

    private void savePhenology() {

        // 1) Verify if date is not empty
        String currentDateValue = mDateEditText.getText().toString().trim();
        if (!currentDateValue.isEmpty()) {

            // 2) Determine which mode is activate (MANUAL or AUTO)
            if (mIsAutoModeEnabled) {
                Log.i(TAG, "The AUTO mode is enabled.");
                //Verify if all fields aren't empty.
                String currentLatitudeDecimal = mLatitudeEditText.getText().toString().trim();
                String currentLongitudeDecimal = mLongitudeEditText.getText().toString().trim();
                String currentSelectedSpecies = mTextViewSelectedSpecies.getText().toString().trim();
                if (!currentLatitudeDecimal.isEmpty() && !currentLongitudeDecimal.isEmpty() && !currentSelectedSpecies.isEmpty()) {
                    //Verify if the data is valid.
                    String[] currentLatitudeSplitArray = currentLatitudeDecimal.split("\\.");
                    String[] currentLongitudeSplitArray = currentLongitudeDecimal.split("\\.");
                    if (currentLatitudeSplitArray.length == 2 && currentLongitudeSplitArray.length == 2) {
                        insertInDatabaseAutoMode(currentDateValue, currentLatitudeDecimal, currentLongitudeDecimal, currentSelectedSpecies);
                    } else {
                        Log.e(TAG, "Las coordenadas no son validas, es imposible guardar.");
                    }
                    ////////////////////////////////
                } else {
                    Log.e(TAG, "Dejaste vacío algun campo, es imposible guardar.");
                }
            } else {
                Log.i(TAG, "The MANUAL mode is enabled");
                //Verify if all fields aren't empty.
                String currentLatitudeDegrees = mLatitudeDegreesEditText.getText().toString().trim();
                String currentLatitudeMinutes = mLatitudeMinutesEditText.getText().toString().trim();
                String currentLatitudeSeconds = mLatitudeSecondsEditText.getText().toString().trim();
                String currentLongitudeDegrees = mLongitudeDegreesEditText.getText().toString().trim();
                String currentLongitudeMinutes = mLongitudeMinutesEditText.getText().toString().trim();
                String currentLongitudeSeconds = mLongitudeSecondsEditText.getText().toString().trim();
                String currentSelectedSpecies = mTextViewSelectedSpecies.getText().toString().trim();
                if (
                        !currentLatitudeDegrees.isEmpty() &&
                                !currentLatitudeMinutes.isEmpty() &&
                                !currentLatitudeSeconds.isEmpty() &&
                                !currentLongitudeDegrees.isEmpty() &&
                                !currentLongitudeMinutes.isEmpty() &&
                                !currentLongitudeSeconds.isEmpty() &&
                                isSpinnerEstadoSelected &&
                                isAutoCompleteTextViewMunicipioSelected &&
                                !currentSelectedSpecies.isEmpty()
                        ) {
                    //Verify if the data is valid.
                    Log.i(TAG, "All the fields aren't empty.");
                    //1) Validate coordinates with ranges.
                    //Pendiente...

                    //2) Concatenate the GMS Coordinates
                    String latitudGMS = currentLatitudeDegrees + "," + currentLatitudeMinutes + "," + currentLatitudeSeconds;
                    String longitudGMS = currentLongitudeDegrees + "," + currentLongitudeMinutes + "," + currentLongitudeSeconds;
                    String idEstado = selectedIdEstado;
                    String idMunicipio = currentIdMunicipio;

                    //3) Convert GMS to Decimal with the function 'convertGMS2GD'.
                    String latitudeDec = convertGMS2GD(currentLatitudeDegrees, currentLatitudeMinutes, currentLatitudeSeconds, "N");
                    String longitudeDec = convertGMS2GD(currentLongitudeDegrees, currentLongitudeMinutes, currentLongitudeSeconds, "O");
                    Log.i(TAG, "Latitude in decimal: " + latitudeDec);
                    Log.i(TAG, "Longitude in decimal: " + longitudeDec);
                    Log.i(TAG, "ID Estado: " + idEstado);
                    Log.i(TAG, "ID Municipio: " + idMunicipio);

                    //4) Insert against ContentProvider with function insertInDatabaseManualMode().
                    insertInDatabaseManualMode(currentDateValue, latitudeDec, longitudeDec, latitudGMS, longitudGMS, idEstado, idMunicipio, currentSelectedSpecies);

                    ///////////////////////////////
                } else {
                    Log.e(TAG, "Dejaste vacío algun campo, es imposible guardar.");
                }
            }
        } else {
            Log.e(TAG, "La fecha esta vacía, es imposible guardar.");
        }

    }

    /* ---------------------------------------------------------------------------------------- */

    private String convertGMS2GD(String degrees, String minutes, String seconds, String zone) {
        String result = null;
        double degreesInDouble = Double.parseDouble(degrees);
        double minutesInDouble = Double.parseDouble(minutes);
        double secondsInDouble = Double.parseDouble(seconds);
        if (zone.equals("N") || zone.equals("E")) {
            double dd = (secondsInDouble / 3600) + (minutesInDouble / 60) + degreesInDouble;
            result = String.valueOf(dd);
        } else if (zone.equals("S") || zone.equals("O")) {
            double dd = -((secondsInDouble / 3600) + (minutesInDouble / 60) + degreesInDouble);
            result = String.valueOf(dd);
        }
        return result;
    }

    private void insertInDatabaseAutoMode(String dateString, String latitudeString, String longitudeString, String selectedSpeciesString) {
        ContentValues values = new ContentValues();
        values.put(PrincipalEntry.COLUMN_FECHA, dateString);
        values.put(PrincipalEntry.COLUMN_LATITUD, latitudeString);
        values.put(PrincipalEntry.COLUMN_LONGITUD, longitudeString);
        values.put(PrincipalEntry.COLUM_ID_USUARIO, currentUserId);
        Uri newUri = getContentResolver().insert(PrincipalEntry.CONTENT_URI, values);
        if (newUri != null) {
            String lastInsertedId = newUri.getLastPathSegment();
            Log.i(TAG, "Last path segment: " + lastInsertedId);
            //Split by pairs (|)
            String[] pairs = selectedSpeciesString.split("\\|");
            int numberOfPairs = pairs.length;
            int counterSuccessInserts = 0;
            Log.i(TAG, "Number of pairs: " + numberOfPairs);
            if (numberOfPairs > 0) {
                ContentValues secondValues = new ContentValues();
                Uri secondUri;
                String currentPair;
                String[] separate;
                for (String pair : pairs) {
                    currentPair = pair;
                    separate = currentPair.split(",");
                    if (separate.length == 2) {
                        Log.i(TAG, "id_especie = " + separate[0] + " id_fenologia = " + separate[1]);
                        secondValues.put(RegistroEntry.COLUMN_ID_PRINCIPAL, lastInsertedId);
                        secondValues.put(RegistroEntry.COLUMN_ID_ESPECIE, separate[0]);
                        secondValues.put(RegistroEntry.COLUMN_ID_FENOLOGIA, separate[1]);
                        secondUri = getContentResolver().insert(RegistroEntry.CONTENT_URI, secondValues);
                        if (secondUri != null) {
                            Log.i(TAG, "secondUri success");
                            counterSuccessInserts++;
                        }
                    }
                }
            }
            //Check number of success inserts
            if (numberOfPairs == counterSuccessInserts) {
                Toast.makeText(this, getString(R.string.form_insert_principal_successful), Toast.LENGTH_SHORT).show();
            }
            finish();
        } else {
            Log.e(TAG, "Falló al insertar el registro.");
        }
    }

    private void insertInDatabaseManualMode(String dateString, String latitudeDecString, String longitudeDecString, String latitudeGMS, String longitudeGMS, String idEstado, String idMunicipio, String selectedSpeciesString) {
        ContentValues values = new ContentValues();
        values.put(PrincipalEntry.COLUMN_FECHA, dateString);
        values.put(PrincipalEntry.COLUMN_LATITUD, latitudeDecString);
        values.put(PrincipalEntry.COLUMN_LONGITUD, longitudeDecString);

        values.put(PrincipalEntry.COLUMN_LATITUD_GMS, latitudeGMS);
        values.put(PrincipalEntry.COLUMN_LONGITUD_GMS, longitudeGMS);
        values.put(PrincipalEntry.COLUMN_CVE_ESTADO, idEstado);
        values.put(PrincipalEntry.COLUMN_CVE_MUNICIPIO, idMunicipio);

        values.put(PrincipalEntry.COLUM_ID_USUARIO, currentUserId);
        Uri newUri = getContentResolver().insert(PrincipalEntry.CONTENT_URI, values);
        if (newUri != null) {
            String lastInsertedId = newUri.getLastPathSegment();
            Log.i(TAG, "Last path segment: " + lastInsertedId);
            //Split by pairs (|)
            String[] pairs = selectedSpeciesString.split("\\|");
            int numberOfPairs = pairs.length;
            int counterSuccessInserts = 0;
            Log.i(TAG, "Number of pairs: " + numberOfPairs);
            if (numberOfPairs > 0) {
                ContentValues secondValues = new ContentValues();
                Uri secondUri;
                String currentPair;
                String[] separate;
                for (String pair : pairs) {
                    currentPair = pair;
                    separate = currentPair.split(",");
                    if (separate.length == 2) {
                        Log.i(TAG, "id_especie = " + separate[0] + " id_fenologia = " + separate[1]);
                        secondValues.put(RegistroEntry.COLUMN_ID_PRINCIPAL, lastInsertedId);
                        secondValues.put(RegistroEntry.COLUMN_ID_ESPECIE, separate[0]);
                        secondValues.put(RegistroEntry.COLUMN_ID_FENOLOGIA, separate[1]);
                        secondUri = getContentResolver().insert(RegistroEntry.CONTENT_URI, secondValues);
                        if (secondUri != null) {
                            Log.i(TAG, "secondUri success");
                            counterSuccessInserts++;
                        }
                    }
                }
            }
            //Check number of success inserts
            if (numberOfPairs == counterSuccessInserts) {
                Toast.makeText(this, getString(R.string.form_insert_principal_successful), Toast.LENGTH_SHORT).show();
            }
            finish();
        } else {
            Log.e(TAG, "Falló al insertar el registro.");
        }
    }

    public void setupAutoCompleteEspecies() {
        ArrayList<Especie> especies = new ArrayList<>();
        Cursor cursor = getContentResolver().query(EspeciesEntry.CONTENT_URI, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                especies.add(new Especie(cursor.getInt(cursor.getColumnIndex(EspeciesEntry.COLUMN_ID_NUM)), cursor.getString(cursor.getColumnIndex(EspeciesEntry.COLUMN_NOMBRE_CIENTIFICO)), cursor.getString(cursor.getColumnIndex(EspeciesEntry.COLUMN_NOMBRE_COMUN))));
            } while (cursor.moveToNext());
            cursor.close();
        }
        autoCompleteAdapter = new AutoCompleteAdapter(this, especies);
        mSpeciesAutoCompleteTextView.setAdapter(autoCompleteAdapter);
        mSpeciesAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Especie currentEspecie = autoCompleteAdapter.getItem(position);
                isAutoCompleteTextViewEspecieSelected = true;
                if (currentEspecie != null) {
                    currentIdEspecie = currentEspecie.getIdNum();
                    currentTextNombreCientifico = currentEspecie.getNombreCientifico();
                    currentTextNombreComun = currentEspecie.getNombreComun();
                    // Show the DialogFragment to select the phenology states.
                    showDialogFragment();


                }
            }
        });
        mSpeciesAutoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                isAutoCompleteTextViewEspecieSelected = false;
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    private void showDialogFragment() {
        // 1) Create AlertDialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Seleccione la etapa fenológica");
        builder.setPositiveButton("Aceptar", null);
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (dialog != null) {
                    mSpeciesAutoCompleteTextView.setText("");
                    dialog.dismiss();
                }

            }
        });

        // 2) Save the content of 'mTextViewSelectedSpecies' into a String variable to extract the
        // phenology ids and save in a StringBuilder.
        String currentSpeciesString = mTextViewSelectedSpecies.getText().toString().trim();
        Log.i(TAG, "currentSpeciesString: " + currentSpeciesString); // 101,8|101,9
        Cursor cursor;
        if (!currentSpeciesString.isEmpty()) {
            Log.i(TAG, "not empty");
            String[] currentSS = currentSpeciesString.split("\\|");
            StringBuilder stringBuilder = new StringBuilder();
            int flagFirst = 0;
            for (String singlePair : currentSS) {
                String[] singlePairArray = singlePair.split(",");
                int singlePairIdEspecie = Integer.parseInt(singlePairArray[0]);
                int singlePairIdFenologia = Integer.parseInt(singlePairArray[1]);
                Log.i(TAG, "singlePairIdEspecie: " + singlePairIdEspecie + " - singlePairIdFenologia: " + singlePairIdFenologia);
                if (currentIdEspecie == singlePairIdEspecie) {
                    if (flagFirst == 0) {
                        flagFirst++;
                    } else {
                        stringBuilder.append(",");
                    }
                    stringBuilder.append(singlePairIdFenologia);
                }
            }
            Log.i(TAG, "Estados fenologicos a omitir: " + stringBuilder.toString());
            cursor = getContentResolver().query(FenologiaEntry.CONTENT_URI, null, stringBuilder.toString(), null, null);
        } else {
            cursor = getContentResolver().query(FenologiaEntry.CONTENT_URI, null, null, null, null);
        }

        // 3) Make a query (with ContentProvider) and add the cursor results to an ArrayList.
        final ArrayList<EtapaFenologica> etapaFenologicas = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                etapaFenologicas.add(new EtapaFenologica(cursor.getInt(cursor.getColumnIndex(FenologiaEntry.COLUMN_ID_FENOLOGIA)), cursor.getString(cursor.getColumnIndex(FenologiaEntry.COLUMN_ETAPA))));
            } while (cursor.moveToNext());
            cursor.close();
        }

        //4) Create an Adapter to pass to the AlertDialog with the recently created ArrayList.
        final FormDialogCustomAdapter formDialogCustomAdapter = new FormDialogCustomAdapter(this, etapaFenologicas);
        builder.setAdapter(formDialogCustomAdapter, null);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        //5) Change the click action of the POSITIVE Button.
        Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Process all selected checkbox, and pass to the resultTextView.
                ArrayList<String> returnedIds = formDialogCustomAdapter.getAllChecked();
                //Log.i(TAG, "IDS Collected: " + returnedIds);
                Log.i(TAG, "IDS Collected Size: " + returnedIds.size());
                if (returnedIds.size() < 1) {
                    Toast.makeText(getApplicationContext(), "Debes seleccionar al menos una etapa", Toast.LENGTH_SHORT).show();
                } else {
                    Log.i(TAG, "Everything OK, continue...");
                    for (String s : returnedIds) {
                        Log.i(TAG, "Row - idEspecie: " + currentIdEspecie + " idFenologia: " + s);
                        // Split the ArrayList of the Adapter and create two variables (idFenologia, TextoFenologia).
                        String[] pairs = s.split("\\|");
                        String idFenologia = pairs[0];
                        String textFenologia = pairs[1];
                        Log.i(TAG, "idFenologia: " + idFenologia + " textoFenologia: " + textFenologia);

                        //Inherited from button (mButtonAddSpecies).
                        mFormHasChanged = true;
                        //Insert objects in RecyclerView
                        //Log.i(TAG, "ROW -> nCientifico:" + currentTextNombreCientifico + " nComun: " + currentTextNombreComun + " etapaFen: " + etapaFenologicas.get);
                        mRecyclerViewForm.setVisibility(View.VISIBLE);
                        especieFenologias.add(new EspecieFenologia(currentTextNombreCientifico, currentTextNombreComun, textFenologia));
                        recyclerViewAdapter.notifyDataSetChanged();
                        int currentItemsInRv = recyclerViewAdapter.getItemCount();
                        mNumEspeciesRvFormTextView.setText(getString(R.string.placeholder_numespecies_rv, currentItemsInRv));


                        currentSelectedSpecies = mTextViewSelectedSpecies.getText().toString().trim();
                        if (currentSelectedSpecies.isEmpty()) {
                            Log.i(TAG, "The concat TextView are empty!");
                            //mTextViewSelectedSpecies.setText(String.valueOf(currentIdEspecie));
                            mTextViewSelectedSpecies.setText(getString(R.string.placeholder_pair_comma, String.valueOf(currentIdEspecie), String.valueOf(idFenologia)));
                        } else {
                            Log.i(TAG, "You have data in the concat TextView");
                            //mTextViewSelectedSpecies.setText(currentSelectedSpecies + "," + currentIdEspecie);
                            String newPair = getString(R.string.placeholder_pair_comma, String.valueOf(currentIdEspecie), String.valueOf(idFenologia));
                            mTextViewSelectedSpecies.setText(getString(R.string.placeholder_pair_bracket, currentSelectedSpecies, newPair));
                        }
                        mSpeciesAutoCompleteTextView.setText("");
                        //Hide keyboard
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (imm != null) {
                            imm.hideSoftInputFromWindow(mSpeciesAutoCompleteTextView.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                        }
                        mSpeciesAutoCompleteTextView.setText("");
                        alertDialog.dismiss();
                    }
                }
            }
        });
    }

    /* ---------------------------------------------------------------------------------------- */

    @Override
    public void update(int itemsCount, int itemRemoved) {
        mNumEspeciesRvFormTextView.setText(getString(R.string.placeholder_numespecies_rv, itemsCount));
        Log.i(TAG, "itemRemoved: " + itemRemoved);
        String currentSpeciesString = mTextViewSelectedSpecies.getText().toString();


        Log.d(TAG, "currentSpeciesString: " + currentSpeciesString);


        String[] pairs = currentSpeciesString.split("\\|");
        StringBuilder updatedCommaSeparated = new StringBuilder();

        int numberOfPairs = pairs.length;
        Log.i(TAG, "Number of pairs: " + numberOfPairs);
        int flagFirst = 0;
        if (numberOfPairs > 0) {
            for (int i = 0; i < numberOfPairs; i++) {


                if (i != itemRemoved) {
                    // Fix the separator according to the value of "i"
                    if (flagFirst == 0 /*&& i < numberOfPairs - 1*/) {
                        flagFirst++;
                    } else {
                        updatedCommaSeparated.append("|");
                    }

                    updatedCommaSeparated.append(pairs[i]);
                    //updatedCommaSeparated.append("|");
                }


                //Log.i(TAG, "Pair[" + i + "] : " + pairs[i]);


            }
            // Remove item from StringArray in position "itemRemoved"
            Log.i(TAG, "Final String: " + updatedCommaSeparated.toString());
            mTextViewSelectedSpecies.setText(updatedCommaSeparated.toString());


        }
    }

    /* ---------------------------------------------------------------------------------------- */

    private void activateManualMode() {
        mIsAutoModeEnabled = false;
        mLinearLayoutAutoMode.setVisibility(View.GONE);
        mLinearLayoutManualMode.setVisibility(View.VISIBLE);
        //Avoid multiple Toasts
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(this, "No se pudo obtener tu ubicación, captura los datos manualmente.", Toast.LENGTH_SHORT);
        mToast.show();
    }

    private void activateAutoMode() {
        mIsAutoModeEnabled = true;
        mLinearLayoutAutoMode.setVisibility(View.VISIBLE);
        mLinearLayoutManualMode.setVisibility(View.GONE);
    }


}
