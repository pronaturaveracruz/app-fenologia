package com.example.hassim.fenologia.autocomplete;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.example.hassim.fenologia.R;

import java.util.ArrayList;
import java.util.List;

public class AutoCompleteAdapter extends ArrayAdapter<Especie> {

    private final List<Especie> mEspecie;
    private final List<Especie> mEspecieAll;

    public AutoCompleteAdapter(@NonNull Context context, ArrayList<Especie> especies) {
        super(context, 0, especies);
        this.mEspecie = new ArrayList<>(especies);
        this.mEspecieAll = new ArrayList<>(especies);
    }

    @Override
    public int getCount() {
        return mEspecie.size();
    }

    @Nullable
    @Override
    public Especie getItem(int position) {
        return mEspecie.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.autocomplete_item, parent, false);
        }
        Especie especieObject = getItem(position);
        TextView nombreCientifico = convertView.findViewById(R.id.text_view_nombre_cientifico);
        TextView nombreComun = convertView.findViewById(R.id.text_view_nombre_comun);
        nombreCientifico.setText(especieObject.getNombreCientifico());
        nombreComun.setText(especieObject.getNombreComun());
        return convertView;
    }

    @NonNull
    @Override
    public Filter getFilter() {


        return new Filter() {


            @Override
            public CharSequence convertResultToString(Object resultValue) {
                return ((Especie) resultValue).getNombreCientifico();
            }

            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults filterResults = new FilterResults();
                List<Especie> especiesSuggestion = new ArrayList<>();
                if (charSequence != null) {
                    for (Especie especie : mEspecieAll) {
                        //BUSCAR COINCIDENCIAS DE NOMBRE CIENTIFICO Y NOMBRE COMUN A LA VEZ.
                        if (especie.getNombreCientifico().toLowerCase().contains(charSequence.toString().toLowerCase()) ||
                                especie.getNombreComun().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                            especiesSuggestion.add(especie);
                        }

                    }
                    filterResults.values = especiesSuggestion;
                    filterResults.count = especiesSuggestion.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                mEspecie.clear();
                if (filterResults != null && filterResults.count > 0) {
                    for (Object object : (List<?>) filterResults.values) {
                        if (object instanceof Especie) {
                            mEspecie.add((Especie) object);
                        }
                    }
                    notifyDataSetChanged();
                } else if (charSequence == null) {
                    // no filter, add entire original list back in
                    mEspecie.addAll(mEspecieAll);
                    notifyDataSetInvalidated();
                }

            }
        };


    }
}
