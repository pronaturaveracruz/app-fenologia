package com.example.hassim.fenologia.services;

import com.google.gson.annotations.SerializedName;

public class SyncSpeciesResponseModel {
    @SerializedName("success")
    private int status;

    @SerializedName("date_from_server")
    private String dateFromServer;

    @SerializedName("total_rows_server")
    private int totalRowsServer;

    @SerializedName("string_to_insert")
    private String stringToInsert;

    @SerializedName("string_to_update")
    private String stringToUpdate;

    @SerializedName("message")
    private String message;

    public SyncSpeciesResponseModel(int status, String dateFromServer, int totalRowsServer, String stringToInsert, String stringToUpdate, String message) {
        this.status = status;
        this.dateFromServer = dateFromServer;
        this.totalRowsServer = totalRowsServer;
        this.stringToInsert = stringToInsert;
        this.stringToUpdate = stringToUpdate;
        this.message = message;
    }

    //Getters
    public int getStatus() {
        return status;
    }

    public String getDateFromServer() {
        return dateFromServer;
    }

    public int getTotalRowsServer() {
        return totalRowsServer;
    }

    public String getStringToInsert() {
        return stringToInsert;
    }

    public String getStringToUpdate() {
        return stringToUpdate;
    }

    public String getMessage() {
        return message;
    }

}
