package com.example.hassim.fenologia.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.example.hassim.fenologia.R;
import com.example.hassim.fenologia.data.PhenologyContract.*;

import java.io.IOException;
import java.util.Arrays;

import static android.content.Context.MODE_PRIVATE;

public class PhenologyProvider extends ContentProvider {
    public static final String TAG = PhenologyDbHelper.class.getSimpleName();
    private PhenologyDbHelper mDbHelper;

    //Create identifiers pairs (ALL_ROWS & BY_ID) for  for each table
    private static final int PRINCIPAL_ALL = 100;
    private static final int PRINCIPAL_BY_ID = 101;
    private static final int PRINCIPAL_WITH_JOIN_ALL = 102;
    private static final int PRINCIPAL_WITH_JOIN_BY_ID = 103;
    private static final int SPECIES_ALL = 200;
    private static final int SPECIES_BY_ID = 201;
    private static final int CF_REGISTRO_ALL = 300;
    private static final int CF_REGISTRO_BY_ID = 301;
    private static final int FENOLOGIA_ALL = 401;
    private static final int FENOLOGIA_BY_ID = 402;
    private static final int UBICACION_ALL = 501;
    private static final int UBICACION_BY_ID = 502;
    private static final int SYNC_LOG_ALL = 601;
    private static final int SYNC_LOG_BY_ID = 602;

    //Add all matchers in switch statement for each method (query, insert, update, delete)
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sUriMatcher.addURI(PhenologyContract.CONTENT_AUTHORITY, PhenologyContract.PATH_PRINCIPAL, PRINCIPAL_ALL);
        sUriMatcher.addURI(PhenologyContract.CONTENT_AUTHORITY, PhenologyContract.PATH_PRINCIPAL + "/#", PRINCIPAL_BY_ID);
        sUriMatcher.addURI(PhenologyContract.CONTENT_AUTHORITY, PhenologyContract.PATH_PRINCIPAL_REGISTROS, PRINCIPAL_WITH_JOIN_ALL);
        sUriMatcher.addURI(PhenologyContract.CONTENT_AUTHORITY, PhenologyContract.PATH_PRINCIPAL_REGISTROS + "/#", PRINCIPAL_WITH_JOIN_BY_ID);
        sUriMatcher.addURI(PhenologyContract.CONTENT_AUTHORITY, PhenologyContract.PATH_ESPECIES, SPECIES_ALL);
        sUriMatcher.addURI(PhenologyContract.CONTENT_AUTHORITY, PhenologyContract.PATH_ESPECIES + "/#", SPECIES_BY_ID);
        sUriMatcher.addURI(PhenologyContract.CONTENT_AUTHORITY, PhenologyContract.PATH_REGISTROS, CF_REGISTRO_ALL);
        sUriMatcher.addURI(PhenologyContract.CONTENT_AUTHORITY, PhenologyContract.PATH_REGISTROS + "/#", CF_REGISTRO_BY_ID);
        sUriMatcher.addURI(PhenologyContract.CONTENT_AUTHORITY, PhenologyContract.PATH_FENOLOGIA, FENOLOGIA_ALL);
        sUriMatcher.addURI(PhenologyContract.CONTENT_AUTHORITY, PhenologyContract.PATH_FENOLOGIA + "/#", FENOLOGIA_BY_ID);
        sUriMatcher.addURI(PhenologyContract.CONTENT_AUTHORITY, PhenologyContract.PATH_UBICACION, UBICACION_ALL);
        sUriMatcher.addURI(PhenologyContract.CONTENT_AUTHORITY, PhenologyContract.PATH_UBICACION + "/#", UBICACION_BY_ID);
        sUriMatcher.addURI(PhenologyContract.CONTENT_AUTHORITY, PhenologyContract.PATH_SYNC_LOG, SYNC_LOG_ALL);
        sUriMatcher.addURI(PhenologyContract.CONTENT_AUTHORITY, PhenologyContract.PATH_SYNC_LOG + "/#", SYNC_LOG_BY_ID);
    }

    @Override
    public boolean onCreate() {
        mDbHelper = new PhenologyDbHelper(getContext());
        try {
            mDbHelper.createDataBase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        try {
            mDbHelper.openDataBase();
        } catch (SQLException sqle) {
            throw sqle;
        }
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        //mDbHelper.close(); //TODO: Commented because cause crash: [Steps to reproduce crash] 1) Turn on WIFI 2) Add new registry 3) View details 4) Press Back Button
        SQLiteDatabase database = mDbHelper.getReadableDatabase();
        Cursor cursor;
        SQLiteQueryBuilder mQueryBuilder = new SQLiteQueryBuilder();

        //Experimental -> Add current user to the query to display only his/her rows.
        SharedPreferences myShared = getContext().getSharedPreferences("FENOLOGIA_PREFS", MODE_PRIVATE);
        int currentUser = myShared.getInt("PREF_USER_ID", 0);

        String rawQuery = "( SELECT ok.*, " +
                //"(SELECT COUNT(*) FROM sisresem_cf_registro WHERE sisresem_cf_registro.id_principal = ok._id) AS num_especies " +
                "(SELECT COUNT(*) FROM sisresem_cf_registro WHERE sisresem_cf_registro.id_principal = ok._id) AS num_especies, " +
                "(SELECT group_concat(id_especie || \",\" || id_fenologia, \"|\") FROM sisresem_cf_registro WHERE id_principal = ok._id GROUP BY id_principal)AS especies_concat " +
                "FROM(SELECT * FROM sisresem_cf_principal) AS ok " +
                "INNER JOIN sisresem_cf_registro ON ok._id = sisresem_cf_registro.id_principal WHERE id_usuario = '" + currentUser + "' )AS ok2 GROUP BY _id ";

        String rawQueryForDetails = "( SELECT * FROM ( SELECT ok.*, " +
                "(SELECT COUNT(*) FROM sisresem_cf_registro WHERE sisresem_cf_registro.id_principal = ok._id) AS num_especies, " +
                "(SELECT group_concat(id_especie || \",\" || id_fenologia, \"|\") FROM sisresem_cf_registro WHERE id_principal = ok._id GROUP BY id_principal)AS especies_concat, " +
                "(SELECT group_concat(nombre_cientifico || \",\" || nombre_comun || \",\" || etapa, \"|\") FROM sisresem_cf_registro INNER JOIN especies ON sisresem_cf_registro.id_especie = especies.id_num INNER JOIN sisresem_cf_fenologia ON sisresem_cf_registro.id_fenologia = sisresem_cf_fenologia.id_fenologia WHERE id_principal = ok._id GROUP BY id_principal )AS especies_text_concat " +
                "FROM(SELECT * FROM sisresem_cf_principal) AS ok " +
                "INNER JOIN sisresem_cf_registro ON ok._id = sisresem_cf_registro.id_principal WHERE id_usuario = '" + currentUser + "' )AS ok2 GROUP BY _id) AS ok3 ";

        int match = sUriMatcher.match(uri);
        switch (match) {
            case PRINCIPAL_ALL:
                cursor = database.query(
                        PrincipalEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case PRINCIPAL_BY_ID:
                selection = PrincipalEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                cursor = database.query(PrincipalEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;


            case PRINCIPAL_WITH_JOIN_ALL:
                Log.i(TAG, "PRINCIPAL_WITH_JOIN_ALL");
                mQueryBuilder.setTables(rawQuery);
                cursor = mQueryBuilder.query(database, projection, selection, selectionArgs, null, null, sortOrder);
                break;

            case PRINCIPAL_WITH_JOIN_BY_ID:
                Log.i(TAG, "PRINCIPAL_WITH_JOIN_BY_ID");
                long id = ContentUris.parseId(uri);
                mQueryBuilder.setTables(rawQueryForDetails);
                mQueryBuilder.appendWhere("_id = " + id);
                cursor = mQueryBuilder.query(database, projection, selection, selectionArgs, null, null, sortOrder);
                break;


            case SPECIES_ALL:
                cursor = database.query(
                        EspeciesEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case SPECIES_BY_ID:
                selection = EspeciesEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                cursor = database.query(EspeciesEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case FENOLOGIA_ALL:
                //NEW WITH RAW
                String rawEtapasFenologicas = "sisresem_cf_fenologia";
                mQueryBuilder.setTables(rawEtapasFenologicas);
                if (selection != null) {
                    mQueryBuilder.appendWhere("_id NOT IN (" + selection + ")");
                }
                cursor = mQueryBuilder.query(database, projection, null, null, null, null, sortOrder);
                break;
            case UBICACION_ALL:
                String groupBy;
                if (selection == null) {
                    groupBy = "nom_estado, id_estado";
                } else {
                    groupBy = "id_municipio,id_estado";
                }
                mQueryBuilder.setTables(UbicacionEntry.TABLE_NAME);
                cursor = mQueryBuilder.query(database, projection, selection, selectionArgs, groupBy, null, sortOrder);
                break;
            case SYNC_LOG_ALL:
                cursor = database.query(
                        SyncLogEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Cannot query unknown URI " + uri);
        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case PRINCIPAL_ALL:
                return PrincipalEntry.CONTENT_LIST_TYPE;
            case PRINCIPAL_BY_ID:
                return PrincipalEntry.CONTENT_ITEM_TYPE;
            case PRINCIPAL_WITH_JOIN_ALL:
                return PrincipalEntry.CONTENT_LIST_TYPE2;
            case PRINCIPAL_WITH_JOIN_BY_ID:
                return PrincipalEntry.CONTENT_ITEM_TYPE2;
            case SPECIES_ALL:
                return EspeciesEntry.CONTENT_LIST_TYPE;
            case SPECIES_BY_ID:
                return EspeciesEntry.CONTENT_ITEM_TYPE;
            case CF_REGISTRO_ALL:
                return RegistroEntry.CONTENT_LIST_TYPE;
            case CF_REGISTRO_BY_ID:
                return RegistroEntry.CONTENT_ITEM_TYPE;
            case FENOLOGIA_ALL:
                return FenologiaEntry.CONTENT_LIST_TYPE;
            case FENOLOGIA_BY_ID:
                return FenologiaEntry.CONTENT_ITEM_TYPE;
            case UBICACION_ALL:
                return UbicacionEntry.CONTENT_LIST_TYPE;
            case UBICACION_BY_ID:
                return UbicacionEntry.CONTENT_ITEM_TYPE;
            case SYNC_LOG_ALL:
                return SyncLogEntry.CONTENT_LIST_TYPE;
            case SYNC_LOG_BY_ID:
                return SyncLogEntry.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalStateException("Unknown URI " + uri + " with match " + match);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case PRINCIPAL_ALL:
                return insertInPrincipal(uri, contentValues);
            case CF_REGISTRO_ALL:
                return insertInRegistro(uri, contentValues);
            case SPECIES_ALL:
                return insertInSpecies(uri, contentValues);
            case SYNC_LOG_ALL:
                return insertInSyncLog(uri, contentValues);
            default:
                throw new IllegalArgumentException("Insert is not supported for " + uri);
        }
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String selection, @Nullable String[] selectionArgs) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case PRINCIPAL_ALL:
                return updatePrincipal(uri, contentValues, selection, selectionArgs);
            case PRINCIPAL_BY_ID:
                selection = PrincipalEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                return updatePrincipal(uri, contentValues, selection, selectionArgs);
            case SPECIES_ALL:
                return updateSpecies(uri, contentValues, selection, selectionArgs);
            case SPECIES_BY_ID:
                selection = PrincipalEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                return updateSpecies(uri, contentValues, selection, selectionArgs);
            default:
                throw new IllegalArgumentException("Update is not supported for " + uri);
        }
    }

    /* ----------------------------------------------------------------------------------------- */

    private Uri insertInPrincipal(Uri uri, ContentValues contentValues) {
        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        long newRowId = database.insert(
                PrincipalEntry.TABLE_NAME,
                null,
                contentValues
        );
        if (newRowId == -1) {
            Log.e(TAG, "Failed to insert row for " + uri);
            return null;
        }
        getContext().getContentResolver().notifyChange(PrincipalEntry.CONTENT_URI_WITH_JOIN, null);
        return ContentUris.withAppendedId(uri, newRowId);
    }

    private Uri insertInRegistro(Uri uri, ContentValues contentValues) {
        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        long newRowId = database.insert(
                RegistroEntry.TABLE_NAME,
                null,
                contentValues
        );
        if (newRowId == -1) {
            Log.e(TAG, "Failed to insert row for " + uri);
            return null;
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return ContentUris.withAppendedId(uri, newRowId);
    }

    private Uri insertInSpecies(Uri uri, ContentValues contentValues) {
        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        long newRowId = database.insert(
                EspeciesEntry.TABLE_NAME,
                null,
                contentValues
        );
        if (newRowId == -1) {
            Log.e(TAG, "Failed to insert row for " + uri);
            return null;
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return ContentUris.withAppendedId(uri, newRowId);
    }

    private Uri insertInSyncLog(Uri uri, ContentValues contentValues) {
        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        long newRowId = database.insert(
                SyncLogEntry.TABLE_NAME,
                null,
                contentValues
        );
        if (newRowId == -1) {
            Log.e(TAG, "Failed to insert row for " + uri);
            return null;
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return ContentUris.withAppendedId(uri, newRowId);
    }

    /* ---------------------------------------------------------------------------------------- */


    private int updatePrincipal(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        if (contentValues.containsKey(PrincipalEntry.COLUMN_SYNC_STATUS)) {
            Integer syncStatus = contentValues.getAsInteger(PrincipalEntry.COLUMN_SYNC_STATUS);
            if (syncStatus == null) {
                throw new IllegalArgumentException("COLUMN_SYNC_STATUS can't be an empty value");
            }
        }
        if (contentValues.size() == 0) {
            return 0;
        }
        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        int rowsUpdated = database.update(PrincipalEntry.TABLE_NAME, contentValues, selection, selectionArgs);
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(PrincipalEntry.CONTENT_URI_WITH_JOIN, null);
        }
        return rowsUpdated;
    }

    private int updateSpecies(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        if (contentValues.size() == 0) {
            return 0;
        }
        SQLiteDatabase database = mDbHelper.getWritableDatabase();
        int rowsUpdated = database.update(EspeciesEntry.TABLE_NAME, contentValues, selection, selectionArgs);
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(EspeciesEntry.CONTENT_URI, null);
        }
        return rowsUpdated;
    }


}
