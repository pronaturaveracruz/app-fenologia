package com.example.hassim.fenologia.internet;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.hassim.fenologia.data.PhenologyContract;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.example.hassim.fenologia.data.PhenologyContract.*;
import com.example.hassim.fenologia.login.SessionPrefs;

public class JobSchedulerService extends JobService {

    //TIP: Use an AsyncTask/Loader to avoid overload the Main Thread, and if so return: true -> Otherwise return: false
    public static final String TAG = JobSchedulerService.class.getName();
    private ApiService mApiService;
    private ContentResolver mContentResolver;

    @Override
    public boolean onStartJob(final JobParameters jobParameters) {
        //Before start ALWAYS verify if some user is logged to perform the Sync
        if (SessionPrefs.get(this).isLoggedIn()) {
            Log.i(TAG, "Congrats, the user is logged in! :D go ahead!!!");
            Log.i(TAG, "onStartJob");
            mApiService = ApiClient.getClient().create(ApiService.class);
            mContentResolver = getContentResolver();
            Cursor myCursor = mContentResolver.query(
                    PrincipalEntry.CONTENT_URI_WITH_JOIN,
                    null,
                    null,
                    null,
                    null
            );
            while (myCursor != null && myCursor.moveToNext()) {


                int currentSyncStatus = myCursor.getInt(myCursor.getColumnIndex(PrincipalEntry.COLUMN_SYNC_STATUS));
                Log.i(TAG, "currentSyncStatus: " + currentSyncStatus);
                ///////////////////////////////////////////////////
                //CASE 1: NOT_SYNC
                if (currentSyncStatus == PhenologyContract.STATUS_NOT_SYNC) {
                    final long currentLocalID = myCursor.getLong(myCursor.getColumnIndex(PrincipalEntry._ID));
                    final String fecha = myCursor.getString(myCursor.getColumnIndex(PrincipalEntry.COLUMN_FECHA));
                    final String latitud = myCursor.getString(myCursor.getColumnIndex(PrincipalEntry.COLUMN_LATITUD));
                    final String longitud = myCursor.getString(myCursor.getColumnIndex(PrincipalEntry.COLUMN_LONGITUD));
                    final String especies_concat = myCursor.getString(myCursor.getColumnIndex(PrincipalEntry.COLUMN_ESPECIES_CONCAT));
                    final int currentUserId = myCursor.getInt(myCursor.getColumnIndex(PrincipalEntry.COLUM_ID_USUARIO));
                    // Determine if manual columns must be inserted
                    boolean c1 = myCursor.isNull(myCursor.getColumnIndex(PrincipalEntry.COLUMN_LATITUD_GMS));
                    boolean c2 = myCursor.isNull(myCursor.getColumnIndex(PrincipalEntry.COLUMN_LONGITUD_GMS));
                    boolean c3 = myCursor.isNull(myCursor.getColumnIndex(PrincipalEntry.COLUMN_CVE_ESTADO));
                    boolean c4 = myCursor.isNull(myCursor.getColumnIndex(PrincipalEntry.COLUMN_CVE_MUNICIPIO));
                    if (!c1 && !c2 && !c3 && !c4) {
                        Log.i(TAG, "Todas las columnas manuales tienen DATOS - MODO MANUAL.");
                        final String latitudGMS = myCursor.getString(myCursor.getColumnIndex(PrincipalEntry.COLUMN_LATITUD_GMS));
                        final String longitudGMS = myCursor.getString(myCursor.getColumnIndex(PrincipalEntry.COLUMN_LONGITUD_GMS));
                        final String cveEstado = myCursor.getString(myCursor.getColumnIndex(PrincipalEntry.COLUMN_CVE_ESTADO));
                        final String cveMunicipio = myCursor.getString(myCursor.getColumnIndex(PrincipalEntry.COLUMN_CVE_MUNICIPIO));
                        makeCallManualMode(currentLocalID, jobParameters, fecha, latitud, longitud, latitudGMS, longitudGMS, cveEstado, cveMunicipio, especies_concat, currentUserId);
                    } else {
                        Log.i(TAG, "Todas las columnas manuales estan vacías (null). - MODO AUTO");
                        makeCallAutoMode(currentLocalID, jobParameters, fecha, latitud, longitud, especies_concat, currentUserId);
                    }
                }
                ///////////////////////////////////////////////////
                //CASE 2: ERROR (NOT MATCHING COORDINATES)
                else if (currentSyncStatus == PhenologyContract.STATUS_ERROR) {
                    final long currentLocalID = myCursor.getLong(myCursor.getColumnIndex(PrincipalEntry._ID));
                    final long currentRemoteID = myCursor.getLong(myCursor.getColumnIndex(PrincipalEntry.COLUMN_ID_PRINCIPAL));
                    makeCallVerifyIfErrorRowsAreOk(currentLocalID, currentRemoteID, jobParameters);
                }
                ///////////////////////////////////////////////////


            }
            if (myCursor != null && !myCursor.isClosed()) {
                myCursor.close();
            }
        } else {
            Log.i(TAG, "No user logged, sorry try later");
            jobFinished(jobParameters, false);
        }
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        Log.i(TAG, "onStopJob");
        return true;
    }

    /* ---------------------------------------------------------------------------------------- */

    private void makeCallAutoMode(final long currentLocalID, final JobParameters jobParameters, String fecha, String latitud, String longitud, String especies_concat, int currentUserId) {
        Log.i(TAG, "makeCallAutoMode");
        Call<InsertPrincipalResponseModel> call = mApiService.insertInAutoMode(fecha, latitud, longitud, especies_concat, currentUserId);
        //execute() -> Synchronous
        //enqueue() -> Asynchronous
        call.enqueue(new Callback<InsertPrincipalResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<InsertPrincipalResponseModel> call, @NonNull Response<InsertPrincipalResponseModel> response) {
                InsertPrincipalResponseModel insertPrincipalResponseModel = response.body();
                if (insertPrincipalResponseModel != null) {
                    if (insertPrincipalResponseModel.getStatus() == 1) {
                        int remoteID = insertPrincipalResponseModel.getInsertedId();
                        if (remoteID > 0) {
                            Log.i(TAG, "Insert in MyQSL successfully with Remote ID: " + remoteID);
                            //Update sync status in SQLite
                            Uri uri = ContentUris.withAppendedId(PrincipalEntry.CONTENT_URI, currentLocalID);
                            ContentValues contentValues = new ContentValues();
                            contentValues.put(PrincipalEntry.COLUMN_SYNC_STATUS, PhenologyContract.STATUS_SYNC_OK);
                            contentValues.put(PrincipalEntry.COLUMN_ID_PRINCIPAL, remoteID);
                            int rowsUpdated = mContentResolver.update(uri, contentValues, null, null);
                            if (rowsUpdated > 0) {
                                Log.i(TAG, "Updated in SQLite successfully!");
                            } else {
                                Log.i(TAG, "Update in SQLite failed...");
                            }
                        }
                    } else {
                        Log.i(TAG, "Insert in MySQL failed");
                    }
                }
                jobFinished(jobParameters, false);
            }

            @Override
            public void onFailure(@NonNull Call<InsertPrincipalResponseModel> call, @NonNull Throwable t) {
                Log.i(TAG, "Callback failed");
                jobFinished(jobParameters, false);
            }
        });
    }

    private void makeCallManualMode(final long currentLocalID, final JobParameters jobParameters, String fecha, String latitud, String longitud, String latitudGMS, String longitudGMS, String cveEstado, String cveMunicipio, String especies_concat, int currentUserId) {
        Log.i(TAG, "makeCallManualMode");
        Call<InsertPrincipalResponseModel> call = mApiService.insertInManualMode(fecha, latitud, longitud, latitudGMS, longitudGMS, cveEstado, cveMunicipio, especies_concat, currentUserId);
        //Log the parameters passed to PHP in the call.
        Log.i(TAG, "The current parameters passed to PHP in the call are: ");
        Log.i(TAG, "Fecha: " + fecha);
        Log.i(TAG, "Latitud: " + latitud);
        Log.i(TAG, "Longitud: " + longitud);
        Log.i(TAG, "Latitud GMS: " + latitudGMS);
        Log.i(TAG, "Longitud GMS: " + longitudGMS);
        Log.i(TAG, "Clave estado: " + cveEstado);
        Log.i(TAG, "Clave municipio: " + cveMunicipio);
        Log.i(TAG, "Especies concat: " + especies_concat);
        Log.i(TAG, "UserID: " + currentUserId);
        call.enqueue(new Callback<InsertPrincipalResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<InsertPrincipalResponseModel> call, @NonNull Response<InsertPrincipalResponseModel> response) {
                InsertPrincipalResponseModel insertPrincipalResponseModel = response.body();
                if (insertPrincipalResponseModel != null) {
                    Log.i(TAG, "The PHP body is not NULL");
                    Log.i(TAG, "Status = " + insertPrincipalResponseModel.getStatus());
                    Log.i(TAG, "Message = " + insertPrincipalResponseModel.getMessage());
                    Log.i(TAG, "inserted_id = " + insertPrincipalResponseModel.getInsertedId());
                    if (insertPrincipalResponseModel.getStatus() == 1) {
                        int remoteID = insertPrincipalResponseModel.getInsertedId();
                        int syncStatusForSQLite = insertPrincipalResponseModel.getSyncStatus();
                        if (remoteID > 0) {
                            Log.i(TAG, "Insert in MyQSL successfully with Remote ID: " + remoteID + " - The sync_status is: " + syncStatusForSQLite);
                            //Update sync status in SQLite
                            Uri uri = ContentUris.withAppendedId(PrincipalEntry.CONTENT_URI, currentLocalID);
                            ContentValues contentValues = new ContentValues();
                            contentValues.put(PrincipalEntry.COLUMN_SYNC_STATUS, syncStatusForSQLite);
                            contentValues.put(PrincipalEntry.COLUMN_ID_PRINCIPAL, remoteID);


                            //DEBUGGING
                            Log.d("sebugging", "SYNC STATUS TO UPDATE IN SQL: " + syncStatusForSQLite);
                            Log.d("sebugging", "REMOTE ID TO UPDATE IN SQL: " + remoteID);
                            /////////////


                            int rowsUpdated = mContentResolver.update(uri, contentValues, null, null);
                            if (rowsUpdated > 0) {
                                Log.i(TAG, "Updated in SQLite successfully!");
                            } else {
                                Log.i(TAG, "Update in SQLite failed...");
                            }
                        }
                    } else {
                        Log.i(TAG, "Insert in MySQL failed (Status is: 0)");
                    }
                }
                jobFinished(jobParameters, false);
            }

            @Override
            public void onFailure(@NonNull Call<InsertPrincipalResponseModel> call, @NonNull Throwable t) {
                Log.i("sebugging", "Callback failed");
                jobFinished(jobParameters, false);
            }
        });
    }

    private void makeCallVerifyIfErrorRowsAreOk(final long currentLocalID, final long currentRemoteID, final JobParameters jobParameters) {
        Log.i(TAG, "makeCallVerifyIfErrorRowsAreOk");
        Call<InsertPrincipalResponseModel> call = mApiService.verifyIfErrorRowsAreOk(currentRemoteID);
        call.enqueue(new Callback<InsertPrincipalResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<InsertPrincipalResponseModel> call, @NonNull Response<InsertPrincipalResponseModel> response) {
                InsertPrincipalResponseModel insertPrincipalResponseModel = response.body();
                if (insertPrincipalResponseModel != null) {
                    Log.i(TAG, "The PHP body is not NULL");
                    Log.i(TAG, "Status = " + insertPrincipalResponseModel.getStatus());
                    Log.i(TAG, "Message = " + insertPrincipalResponseModel.getMessage());
                    if (insertPrincipalResponseModel.getStatus() == 1) {
                        //Update in SQLite.
                        Uri uri = ContentUris.withAppendedId(PrincipalEntry.CONTENT_URI, currentLocalID);
                        String stringUpdateSQLite = insertPrincipalResponseModel.getStringUpdateSQLite();
                        //Log.i(TAG, "String from PHP: " + stringUpdateSQLite);
                        String[] phpUpdatedValues = stringUpdateSQLite.split("\\|");
                        if (phpUpdatedValues.length == 6) {
                            ContentValues contentValues = new ContentValues();
                            contentValues.put(PrincipalEntry.COLUMN_SYNC_STATUS, PhenologyContract.STATUS_SYNC_OK);
                            String updatedLatDec = phpUpdatedValues[0];
                            String updatedLonDec = phpUpdatedValues[1];
                            String updatedLatGMS = phpUpdatedValues[2];
                            String updatedLonGMS = phpUpdatedValues[3];
                            String updatedCveEstado = phpUpdatedValues[4];
                            String updatedCveMunicipio = phpUpdatedValues[5];
                            contentValues.put(PrincipalEntry.COLUMN_LATITUD, updatedLatDec);
                            contentValues.put(PrincipalEntry.COLUMN_LONGITUD, updatedLonDec);
                            contentValues.put(PrincipalEntry.COLUMN_LATITUD_GMS, updatedLatGMS);
                            contentValues.put(PrincipalEntry.COLUMN_LONGITUD_GMS, updatedLonGMS);
                            contentValues.put(PrincipalEntry.COLUMN_CVE_ESTADO, updatedCveEstado);
                            contentValues.put(PrincipalEntry.COLUMN_CVE_MUNICIPIO, updatedCveMunicipio);
                            int rowsUpdated = mContentResolver.update(uri, contentValues, null, null);
                            if (rowsUpdated > 0) {
                                Log.i(TAG, "Updated in SQLite successfully!");
                            } else {
                                Log.i(TAG, "Update in SQLite failed...");
                            }
                        } else {
                            Log.e(TAG, "Not enough values to update");
                        }
                        ////////////////////////////
                    }
                }
                jobFinished(jobParameters, false);
            }

            @Override
            public void onFailure(@NonNull Call<InsertPrincipalResponseModel> call, @NonNull Throwable t) {
                Log.i(TAG, "Callback failed");
                jobFinished(jobParameters, false);
            }
        });
    }


}
