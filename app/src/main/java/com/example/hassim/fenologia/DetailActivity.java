package com.example.hassim.fenologia;

import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.hassim.fenologia.data.PhenologyContract.*;
import com.example.hassim.fenologia.details.EspecieFenologia;
import com.example.hassim.fenologia.recyclerview.RecyclerViewAdapter;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    public static final String TAG = DetailActivity.class.getSimpleName();
    TextView mDateTextView, mLocationTextView, mNumEspeciesTextView;
    ArrayList<EspecieFenologia> especieFenologias;
    Uri mCurrentUri;
    private static final int DETAIL_LOADER = 1;
    RecyclerView mRecyclerViewDetail;
    RecyclerView.LayoutManager mLayoutManager;
    RecyclerViewAdapter recyclerViewAdapter;
    Button mGoToMapButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //References
        mDateTextView = findViewById(R.id.date_text_view_detail);
        mLocationTextView = findViewById(R.id.location_text_view_detail);
        mNumEspeciesTextView = findViewById(R.id.text_view_num_especies_rv_detail);
        mRecyclerViewDetail = findViewById(R.id.recycler_view_detail);
        mGoToMapButton = findViewById(R.id.go_to_map_button);

        //Setup RecyclerView
        especieFenologias = new ArrayList<>();
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerViewDetail.setLayoutManager(mLayoutManager);
        recyclerViewAdapter = new RecyclerViewAdapter(this, especieFenologias);
        mRecyclerViewDetail.setAdapter(recyclerViewAdapter);

        //Get data from Intent
        Intent intent = getIntent();
        mCurrentUri = intent.getData();
        Log.i(TAG, "ID from the URI: " + String.valueOf(ContentUris.parseId(mCurrentUri)));

        //Start the loader
        getLoaderManager().initLoader(DETAIL_LOADER, null, this);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.i(TAG, "onCreateLoader");
        String[] projection = {
                PrincipalEntry._ID + "," +
                        PrincipalEntry.COLUMN_FECHA + "," +
                        PrincipalEntry.COLUMN_LATITUD + "," +
                        PrincipalEntry.COLUMN_LONGITUD + ", " +
                        PrincipalEntry.COLUMN_NUM_ESPECIES + "," +
                        PrincipalEntry.COLUMN_ESPECIES_TEXT_CONCAT
        };
        return new CursorLoader(this,
                ContentUris.withAppendedId(PrincipalEntry.CONTENT_URI_WITH_JOIN, ContentUris.parseId(mCurrentUri)),
                projection,
                null,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        Log.i(TAG, "onLoadFinished");
        if (cursor != null) {
            Log.i(TAG, "cursor is not NULL");
            while (cursor.moveToNext()) {
                Log.i(TAG, "cursor is moving to NEXT");
                int dateColumnIndex = cursor.getColumnIndex(PrincipalEntry.COLUMN_FECHA);
                int latitudeColumnIndex = cursor.getColumnIndex(PrincipalEntry.COLUMN_LATITUD);
                int longitudeColumnIndex = cursor.getColumnIndex(PrincipalEntry.COLUMN_LONGITUD);
                int numEspeciesColumIndex = cursor.getColumnIndex(PrincipalEntry.COLUMN_NUM_ESPECIES);
                int especiesTextConcatColumIndex = cursor.getColumnIndex(PrincipalEntry.COLUMN_ESPECIES_TEXT_CONCAT);

                String date = cursor.getString(dateColumnIndex);
                final String latitude = cursor.getString(latitudeColumnIndex);
                final String longitude = cursor.getString(longitudeColumnIndex);
                int numEspecies = cursor.getInt(numEspeciesColumIndex);
                String especiesTextConcat = cursor.getString(especiesTextConcatColumIndex);

                Log.i(TAG, "Content of date: " + date);
                Log.i(TAG, "Content of latitude: " + latitude);
                Log.i(TAG, "Content of longitude: " + longitude);
                Log.i(TAG, "Content of numEspecies: " + numEspecies);
                Log.i(TAG, "Content of especiesTextConcat: " + especiesTextConcat);

                //Format date
                String[] fullDate = date.split("-");
                String year = fullDate[0];
                String month = fullDate[1];
                String day = fullDate[2];
                switch (month) {
                    case "01":
                        month = "enero";
                        break;
                    case "02":
                        month = "febrero";
                        break;
                    case "03":
                        month = "marzo";
                        break;
                    case "04":
                        month = "abril";
                        break;
                    case "05":
                        month = "mayo";
                        break;
                    case "06":
                        month = "junio";
                        break;
                    case "07":
                        month = "julio";
                        break;
                    case "08":
                        month = "agosto";
                        break;
                    case "09":
                        month = "septiembre";
                        break;
                    case "10":
                        month = "octubre";
                        break;
                    case "11":
                        month = "noviembre";
                        break;
                    case "12":
                        month = "diciembre";
                        break;
                    default:
                        break;
                }

                mDateTextView.setText(getString(R.string.placeholder_date, day, month, year));
                mLocationTextView.setText(getString(R.string.placeholder_location, latitude, longitude));
                mNumEspeciesTextView.setText(getString(R.string.placeholder_numespecies_rv_detail, numEspecies));


                //Setup the GoToMapButton
                mGoToMapButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i(TAG, "Open Google Maps with the location");
                        Uri gmmIntentUri = Uri.parse("geo:" + latitude + "," + longitude + "?q=" + latitude + "," + longitude + "(Punto fenológico)");
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        if (mapIntent.resolveActivity(getPackageManager()) != null) {
                            startActivity(mapIntent);
                        }
                    }
                });


                String[] singleSpeciesItem = especiesTextConcat.split("\\|");
                Log.i(TAG, "Current length of array: " + singleSpeciesItem.length);
                String[] currentRow;
                for (String aSingleSpeciesItem : singleSpeciesItem) {
                    currentRow = aSingleSpeciesItem.split(",");
                    String currentNombreCientifico = currentRow[0];
                    String currentNombreComun = currentRow[1];
                    String currentEtapaFenologica = currentRow[2];
                    Log.i(TAG, "Current nombre cientifico: " + currentNombreCientifico);
                    especieFenologias.add(new EspecieFenologia(currentNombreCientifico, currentNombreComun, currentEtapaFenologica));
                }
                recyclerViewAdapter.notifyDataSetChanged();
            }
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.i(TAG, "onLoaderReset");
        mLocationTextView.setText("");
        recyclerViewAdapter.notifyItemRangeRemoved(0, recyclerViewAdapter.getItemCount());
        mRecyclerViewDetail.removeAllViewsInLayout();
    }


}
