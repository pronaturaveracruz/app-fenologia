package com.example.hassim.fenologia.spinner;

public class EtapaFenologica {

    private int idFenologia;
    private String etapa;

    public EtapaFenologica(int idFenologia, String etapa) {
        this.idFenologia = idFenologia;
        this.etapa = etapa;
    }

    public int getIdFenologia() {
        return idFenologia;
    }

    public String getEtapa() {
        return etapa;
    }

}
