package com.example.hassim.fenologia;

import android.content.Context;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hassim.fenologia.data.PhenologyContract;
import com.example.hassim.fenologia.data.PhenologyContract.PrincipalEntry;

public class PhenologyCursorAdapter extends CursorAdapter {

    public PhenologyCursorAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate(R.layout.list_item, viewGroup, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        //References
        TextView numberSpeciesCircleTextView = view.findViewById(R.id.number_species_circle_text_view);
        TextView latitudeTextView = view.findViewById(R.id.latitude_text_view);
        TextView longitudeTextView = view.findViewById(R.id.longitude_text_view);
        TextView dateTextView = view.findViewById(R.id.date_text_view);
        ImageView syncStatusImageView = view.findViewById(R.id.sync_status_image_view);

        int dateColumnIndex = cursor.getColumnIndex(PrincipalEntry.COLUMN_FECHA);
        int latitudeColumnIndex = cursor.getColumnIndex(PrincipalEntry.COLUMN_LATITUD);
        int longitudeColumnIndex = cursor.getColumnIndex(PrincipalEntry.COLUMN_LONGITUD);
        int syncStatusColumnIndex = cursor.getColumnIndex(PrincipalEntry.COLUMN_SYNC_STATUS);
        int numEspeciesColumIndex = cursor.getColumnIndex(PrincipalEntry.COLUMN_NUM_ESPECIES);

        String dateString = cursor.getString(dateColumnIndex);
        String latitudeString = cursor.getString(latitudeColumnIndex);
        String longitudeString = cursor.getString(longitudeColumnIndex);
        Integer syncStatus = cursor.getInt(syncStatusColumnIndex);
        Integer numEspecies = cursor.getInt(numEspeciesColumIndex);

        dateTextView.setText(dateString);
        latitudeTextView.setText(latitudeString);
        longitudeTextView.setText(longitudeString);
        if (syncStatus == PhenologyContract.STATUS_SYNC_OK) {
            syncStatusImageView.setImageResource(R.drawable.check_circle);
            syncStatusImageView.setColorFilter(ContextCompat.getColor(context, android.R.color.holo_green_light), PorterDuff.Mode.SRC_IN);
        } else if (syncStatus == PhenologyContract.STATUS_ERROR) {
            syncStatusImageView.setImageResource(R.drawable.alert_octagram);
            syncStatusImageView.setColorFilter(ContextCompat.getColor(context, R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        } else {
            syncStatusImageView.setImageResource(R.drawable.cloud_sync);
            syncStatusImageView.setColorFilter(ContextCompat.getColor(context, android.R.color.darker_gray), PorterDuff.Mode.SRC_IN);
        }
        numberSpeciesCircleTextView.setText(String.valueOf(numEspecies));

    }


}
